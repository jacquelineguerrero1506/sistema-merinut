<!DOCTYPE html>
<html lang="en">

<?php include 'modal_expediente.php';?>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Cards</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">


  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link href="css/sb-admin-2.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
          <img src="img/logoMerinut.png" width="350px" height="auto" />
        </div>
      </a>

      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Administración de Pacientes -->
      <li class="nav-item active">
        <a class="nav-link" href="pacientes.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Administrar Pacientes</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Agenda</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Ingresos/Egresos</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>



          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                      aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter">3+</span>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alerts Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                      <i class="fas fa-donate text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                      <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>

            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                <!-- Counter - Messages -->
                <span class="badge badge-danger badge-counter">7</span>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been
                      having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                  </div>
                  <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them
                      sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so
                      far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people
                      say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Valerie Luna</span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading 
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"><b>Detalle del Paciente:</b>  </h1>
          </div>
-->
         

          <div class="row">

            <div class="col-lg-12">

              <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseCardInfoGeneral" class="d-block card-header py-3" data-toggle="collapse" role="button"
                  aria-expanded="true" aria-controls="collapseCardExample" style="background-color:hotpink">
                  <h6 class="m-0 font-weight-bold text-primary" style="color:white !important">Expediente Clínico</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseCardInfoGeneral">
                  <div class="card-body">
                    
                    <button type="button" class="btn btn-primary" id="btnRegistrarExpediente">ALTA DE EXPEDIENTE</button><br><br>

                    <div class="container">
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link" href="#verInfoGeneralExp" id="tabVerInfoGeneral">Información General</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#verAntecedentesExp" id="tabVerAntecedentes">Antecedentes</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#verSomatometriaExp" id="tabVerSomatometria">Somatometría</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="#verOtrosExp" id="tabVerOtros">Otros</a>
            </li>
          </ul>
        </div>
        
        <br>
                      
        <!-- INFORMACIÓN GENERAL  -->
        <div class="container" id="verInfoGeneralExp">
          <form name="form" class="form-horizontal" >
         
            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Nombre del paciente:</label><br>
                <input type="text" class="form-control solo-letras" id="ver_nombre_paciente_exp">
              </div>
              <div class="col-md-6">
                <label class="lblBold">Edad:</label>
                <input type="text" class="form-control" id="ver_edad_exp">
              </div>
            </div>
            <br>

            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Situación Civil:</label>
                <select class="form-control"  id="ver_situacion_civil_exp">                                
                  <?php foreach($catSituacionCivil as $situacionCivil) {?>
                    <option value="<?php echo $situacionCivil->id_situacion_civil; ?>"><?php echo $situacionCivil->valor; ?></option>                            
                  <?php } ?> 
                </select>
                <br>          
                <label class="lblBold">Embarazada:</label>&nbsp;&nbsp;
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ver_embarazoRadioOptions" id="radioEmbarazo1" value="1">
                    <label class="form-check-label" for="radioEmbarazo1">Sí</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ver_embarazoRadioOptions" id="radioEmbarazo2" value="0">
                    <label class="form-check-label" for="radioEmbarazo2">No</label>
                  </div>
              </div>
              <div class="col-md-6">
                <label class="lblBold">Expectativa:</label><br>
                <textarea class="form-control" id="ver_expectativa_exp" rows="4" cols="40"></textarea>
              </div>
            </div>
            
            <br>
            
            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Actividad Laboral:</label><br>
                <input type="text" class="form-control" id="ver_actividad_laboral_exp">
              </div>
              <div class="col-md-6">
                <label class="lblBold">Descripción Actividad:</label><br>
                <textarea class="form-control" id="ver_desc_actividad_exp" rows="4" cols="40"></textarea>
              </div>
            </div>
          </form>
        </div>
        
        <br>
                               
        <!-- ANTECEDENTES  -->
        <div class="container" id="verAntecedentesExp">
          <form name="form" class="form-horizontal">
            <div class="row">
              <div class="col-md-4">
                <label class="lblBold">Situación Estrés:</label>                            
                <select class="form-control" id="ver_situacion_estres_exp">
                  <?php foreach($catSituacionEstres as $situacionEstres) {?>
                    <option value="<?php echo $situacionEstres->id_estres; ?>"><?php echo $situacionEstres->valor; ?></option>                            
                  <?php } ?>                       
                </select>
              </div>
              <div class="col-md-4">
                <label class="lblBold">Situación Intestinal:</label>                            
                <select class="form-control"  id="ver_situacion_intestinal_exp">
                  <?php foreach($catSituacionIntestinal as $situacionIntestinal) {?>
                    <option value="<?php echo $situacionIntestinal->id_intestinal; ?>"><?php echo $situacionIntestinal->valor; ?></option>                            
                  <?php } ?>                        
                </select>
              </div>
              <div class="col-md-4">
                <label class="lblBold">Situación Sueño:</label>                            
                <select class="form-control"  id="ver_situacion_sueno_exp">
                  <?php foreach($catSituacionSueno as $situacionSueno) {?>
                    <option value="<?php echo $situacionSueno->id_sueno; ?>"><?php echo $situacionSueno->valor; ?></option>                            
                  <?php } ?>                      
                </select>
              </div>
            </div>
                        
            <br>

            <div class="row">
              <div class="col-md-4">
                <label class="lblBold">Complexión:</label><br>
                <input type="text" class="form-control" id="ver_complexion_exp">
              </div>
              <div class="col-md-4">
                <label class="lblBold">Actividad Física:</label><br>                            
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ver_actFisicaRadioOptions" id="radioActFisica1" value="1">
                  <label class="form-check-label" for="radioActFisica1">Sí</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ver_actFisicaRadioOptions" id="radioActFisica2" value="0">
                  <label class="form-check-label" for="radioActFisica2">No</label>
                </div>
              </div>
              <div class="col-md-4">
                <label class="lblBold">Ingesta Agua:</label><br>                            
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ver_ingestaAguaRadioOptions" id="radioIngestaAgua1" value="1">
                  <label class="form-check-label" for="radioIngestaAgua1">Sí</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ver_ingestaAguaRadioOptions" id="radioIngestaAgua2" value="0">
                  <label class="form-check-label" for="radioIngestaAgua2">No</label>
                </div>
              </div>
            </div>

            <br>

            <div class="row">
              <div class="col-md-4">
                <label class="lblBold">Consumo Tabaco:</label><br>                            
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ver_tabacoRadioOptions" id="radioTabaco1" value="1">
                  <label class="form-check-label" for="radioTabaco1">Sí</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ver_tabacoRadioOptions" id="radioTabaco2" value="0">
                  <label class="form-check-label" for="radioTabaco2">No</label>
                </div>
              </div>
              <div class="col-md-4">
                <label class="lblBold">Consumo Drogas:</label><br>                            
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ver_drogasRadioOptions" id="radioDrogas1" value="1">
                  <label class="form-check-label" for="radioDrogas1">Sí</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ver_drogasRadioOptions" id="radioDrogas2" value="0">
                  <label class="form-check-label" for="radioDrogas2">No</label>
                </div>
              </div>
              <div class="col-md-4">
                <label class="lblBold">Consumo Alcohol:</label><br>                            
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ver_alcoholRadioOptions" id="radioAlcohol1" value="1">
                  <label class="form-check-label" for="radioAlcohol1">Sí</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ver_alcoholRadioOptions" id="radioAlcohol2" value="0">
                  <label class="form-check-label" for="radioAlcohol2">No</label>
                </div>
              </div>
            </div>

            <br>

            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Alimentos Favoritos:</label><br>
                <textarea class="form-control" id="ver_alimentos_fav_exp" rows="4" cols="40"></textarea>
              </div>
              <div class="col-md-6">
                <label class="lblBold">Alimentos Rechazados:</label><br>
                <textarea class="form-control" id="ver_alimentos_rech_exp" rows="4" cols="40"></textarea>
              </div>
            </div>

            <br>

            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Control Fisiológico:</label><br>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">Dolor de Pecho</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                  <label class="form-check-label" for="defaultCheck2">Dificultad para respirar</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">Palpitaciones</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                  <label class="form-check-label" for="defaultCheck2">Aumento de Frecuencia Cardiaca</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">Aumento de Presión Arterial</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                  <label class="form-check-label" for="defaultCheck2">Dolor de cabeza y cuello</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">Fatiga</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                  <label class="form-check-label" for="defaultCheck2">Ansioso e Irritable</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">Ansiedad</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                  <label class="form-check-label" for="defaultCheck2">Sudoración</label>
                </div>
              </div>                            
            </div>

            <br>

            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Antecedentes Familiares:</label><br>
                <textarea class="form-control" id="ver_antecedentes_fam_exp"  rows="4" cols="40"></textarea>
              </div>
              <div class="col-md-6">
                <label class="lblBold">Antecedentes Personales:</label><br>
                <textarea class="form-control" id="ver_antecedentes_pers_exp" rows="4" cols="40"></textarea>
              </div>
            </div>
            
          </form>
        </div>

        <br>

        <!-- SOMATOMETRIA -->
        <div class="container" id="verSomatometriaExp">
          <form name="form" class="form-horizontal">
            <div class="row">
              <div class="col-md-3">
                <label class="lblBold">Peso</label><br>
                <input type="text" class="form-control solo-letras" id="ver_peso_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">Altura:</label>
                <input type="text" class="form-control" id="ver_altura_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">Talla:</label>
                <input type="text" class="form-control" id="ver_talla_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">Cadera:</label>
                <input type="text" class="form-control"  id="ver_cadera_exp">
              </div>
            </div>
    
            <br>

            <div class="row">
              <div class="col-md-3">
                <label class="lblBold">Muñeca</label><br>
                <input type="text" class="form-control solo-letras"  id="ver_muneca_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">Cintura:</label>
                <input type="text" class="form-control" id="ver_cintura_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">Pantorrila:</label>
                <input type="text" class="form-control"  id="ver_pantorrilla_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">IMC:</label>
                <input type="text" class="form-control"  id="ver_imc_exp">
              </div>
            </div>
            
            <br>

            <div class="row">
              <div class="col-md-3">
                <label class="lblBold">P. Abdominal</label><br>
                <input type="text" class="form-control solo-letras" id="ver_p_abdominal_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">P. Auxiliar Medio:</label>
                <input type="text" class="form-control" id="ver_p_aux_medio_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">P. Pectoral:</label>
                <input type="text" class="form-control" id="ver_p_pectoral_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">P. Subescapular:</label>
                <input type="text" class="form-control" id="ver_p_subescapular_exp">
              </div>
            </div>
          </form>          
        </div>

        <!-- OTROS -->
        <div class="container" id="VerOtrosExp">
          <form name="form" class="form-horizontal">
            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Etiquetas:</label><br>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <?php foreach($catEtiquetas as $etiquetas) {?>
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="<?php echo $etiquetas->id_cat_etiqueta; ?>" id="ver_defaultCheck<?php echo $etiquetas->id_cat_etiqueta; ?>">
                    <label class="form-check-label" for="ver_defaultCheck<?php echo $etiquetas->id_cat_etiqueta; ?>">
                      <?php echo $etiquetas->nombre; ?>
                    </label>
                  </div>
                <?php } ?> 
              </div>                          
            </div>

            <br>

            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Notas:</label><br>
                <textarea class="form-control" id="ver_notas_exp"  rows="4" cols="40"></textarea>
              </div>
              <div class="col-md-6">
                <label class="lblBold">Observaciones:</label><br>
                <textarea class="form-control" id="ver_observaciones_exp" rows="4" cols="40"></textarea>
              </div>
            </div>

          </form>
        </div>

                  <!--  <strong>Nombre Completo: </strong> Andrea Jacqueline Guerrero Valadez<br>
                    <strong>Fecha de Nacimiento: </strong> 15/Junio/1992<br>
                    <strong>Edad: </strong> 28<br>
                    <strong>Estatura: </strong> 1.56 mts.<br>
                    <strong>Alergias: </strong> N/A<br>
                    <strong>Ejercicio: </strong> Sí<br> -->
                  </div>
                </div>
              </div>

            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">

              <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseCardDieta" class="d-block card-header py-3" data-toggle="collapse" role="button"
                  aria-expanded="true" aria-controls="collapseCardExample" style="background-color: hotpink;">
                  <h6 class="m-0 font-weight-bold text-primary" style="color:white !important">Registro de Dieta</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseCardDieta">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-10">
                      <form name="form" class="form-horizontal" >
                        <div class="row">
                          <div class="col-md-6">
                            <label class="lblBold">DESAYUNO:</label>
                          </div><br>
                          <div class="col-md-6">
                            <label class="lblBold">COLACIÓN 1:</label>
                          </div><br>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <textarea id="verDesayuno"  rows="4" cols="60">
                          </textarea>
                          </div>
                          <div class="col-md-6">
                            <textarea id="verColacion1" rows="4" cols="60">
                          </textarea>
                          </div>
                        </div>

                        <br>

                        <div class="row">
                          <div class="col-md-6">
                            <label class="lblBold">COMIDA:</label>
                          </div><br>
                          <div class="col-md-6">
                            <label class="lblBold">COLACIÓN 2:</label>
                          </div><br>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <textarea id="verComida" rows="4" cols="60">
                          </textarea>
                          </div>
                          <div class="col-md-6">
                            <textarea id="verColacion2" rows="4" cols="60">
                          </textarea>
                          </div>
                        </div>

                        <br>

                        <div class="row">
                          <div class="col-md-6">
                            <label class="lblBold">CENA:</label>
                          </div><br>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <textarea id="verCena" rows="4" cols="60">
                          </textarea>
                          </div>
                        </div>

                        </form>
                      </div>
                      <div class="col-lg-2">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                          data-target="#modalRegistroDieta">Crear nueva dieta</button><br><br>
                        <button type="button" class="btn btn-primary">Imprimir Dieta</button><br>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <!-- Modal Nueva Dieta-->
          <div class="modal fade" id="modalRegistroDieta" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Registrar Dieta</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <form name="form" class="form-horizontal" >


                    <div class="row">
                      <div class="col-md-6">
                        <label class="lblBold">DESAYUNO:</label>
                      </div><br>
                      <div class="col-md-6">
                        <label class="lblBold">COLACIÓN 1:</label>
                      </div><br>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <textarea id="desayuno"  rows="4" cols="40">
                      </textarea>
                      </div>
                      <div class="col-md-6">
                        <textarea id="colacion1" rows="4" cols="40">
                      </textarea>
                      </div>
                    </div>

                    <br>

                    <div class="row">
                      <div class="col-md-6">
                        <label class="lblBold">COMIDA:</label>
                      </div><br>
                      <div class="col-md-6">
                        <label class="lblBold">COLACIÓN 2:</label>
                      </div><br>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <textarea id="comida" rows="4" cols="40">
                      </textarea>
                      </div>
                      <div class="col-md-6">
                        <textarea id="colacion2" rows="4" cols="40">
                      </textarea>
                      </div>
                    </div>

                    <br>

                    <div class="row">
                      <div class="col-md-6">
                        <label class="lblBold">CENA:</label>
                      </div><br>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <textarea id="cena" rows="4" cols="40">
                      </textarea>
                      </div>
                    </div>

                  </form>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  <button type="button" class="btn btn-primary" id="btnGuardarDieta">Guardar</button>
                </div>
              </div>
            </div>
          </div>
          <!--FIN  Modal Nueva Dieta-->

          <!-- Modal Reistro Info de consulta-->
          <div class="modal fade" id="modalRegistroConsultaPaciente" tabindex="-1" role="dialog"
          aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-xl"  role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Registrar Información de Consulta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                <form name="form" class="form-horizontal" method="post" action="registrate.jsp">
                  <div class="row">
                    <div class="col-md-6">
                      <label class="lblBold">Fecha de consulta</label>
                    </div>
                    <div class="col-md-6">
                      <input type="date" class="form-control solo-letras" 
                        id="registro_nombreUsuario" value="2020-07-25" disabled>
                    </div>
                  </div>
                  <hr><br>

                  <div class="row">
                    <div class="col-md-3">
                      <label class="lblBold">Peso:</label>
                    </div>
                    <div class="col-md-3">
                      <label class="lblBold">Altura:</label>
                    </div>
                    <div class="col-md-3">
                      <label class="lblBold">Talla:</label>
                    </div>
                    <div class="col-md-3">
                      <label class="lblBold">Cadera:</label>
                    </div>
                    <br>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_peso">
                    </div>
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_altura">
                    </div>
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_talla">
                    </div>
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_cadera">
                    </div>
                  </div>
                  
                  <br>

                  <div class="row">
                    <div class="col-md-3">
                      <label class="lblBold">Muñeca:</label>
                    </div>
                    <div class="col-md-3">
                      <label class="lblBold">Cintura:</label>
                    </div>
                    <div class="col-md-3">
                      <label class="lblBold">Pantorrilla:</label>
                    </div>
                    <div class="col-md-3">
                      <label class="lblBold">IMC:</label>
                    </div>
                    <br>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_muneca">
                    </div>
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_cintura">
                    </div>
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_pantorrilla">
                    </div>
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_imc">
                    </div>
                  </div>
                  
                  <br>


                  <div class="row">
                    <div class="col-md-3">
                      <label class="lblBold">P. Abdominal:</label>
                    </div>
                    <div class="col-md-3">
                      <label class="lblBold">P. Auxiliar Medio:</label>
                    </div>
                    <div class="col-md-3">
                      <label class="lblBold">P. Pectoral:</label>
                    </div>
                    <div class="col-md-3">
                      <label class="lblBold">P. Subescapular:</label>
                    </div>
                    <br>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_pabdominal">
                    </div>
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_pauxmedio">
                    </div>
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_ppectoral">
                    </div>
                    <div class="col-md-3">
                      <input type="text" class="form-control solo-letras"  id="seg_psubescapular">
                    </div>
                  </div>
                  <br>

                </form>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnGuardarSeguimiento">Guardar</button>
              </div>
            </div>
          </div>
        </div>


          <div class="row">

            <div class="col-lg-12">

              <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseCardHistorial" class="d-block card-header py-3" data-toggle="collapse" role="button"
                  aria-expanded="true" aria-controls="collapseCardExample" style="background-color: hotpink;">
                  <h6 class="m-0 font-weight-bold text-primary" style="color:white !important">Seguimiento</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseCardHistorial">
                  <div class="card-body">
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                      data-target="#modalRegistroConsultaPaciente">Nuevo registro</button><br><br>

                    <div class="table-responsive">
                      <table class="table table-bordered" id="tblSeguimiento" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Fecha Consulta</th>
                            <th>Peso</th>
                            <th>Altura</th>
                            <th>Talla</th>
                            <th>Cadera</th>
                            <th>Muñeca</th>
                            <th>Cintura</th>
                            <th>Pantorrilla</th>
                            <th>IMC</th>
                            <th>P. Abdominal</th>                            
                            <th>P. Auxiliar Medio</th>
                            <th>P. Pectoral</th>
                            <th>P. Subescapular</th>                          
                          </tr>
                        </thead>
                        <tbody>                         
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script src="js/detalle_pacientes.js"></script>

  <script src="vendor/sweetalert2/sweetalert2.all.min.js"></script>

</body>

</html>