<div class="modal fade" id="modalExpediente" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Alta de Expediente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="container">
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link" href="#infoGeneralExp" id="tabInfoGeneral">Información General</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#antecedentesExp" id="tabAntecedentes">Antecedentes</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#somatometriaExp" id="tabSomatometria">Somatometría</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="#otrosExp" id="tabOtros">Otros</a>
            </li>
          </ul>
        </div>
        
        <br>
                      
        <!-- INFORMACIÓN GENERAL  -->
        <div class="container" id="infoGeneralExp">
          <form name="form" class="form-horizontal" >
         
            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Nombre del paciente:</label><br>
                <input type="text" class="form-control solo-letras" id="nombre_paciente_exp">
              </div>
              <div class="col-md-6">
                <label class="lblBold">Edad:</label>
                <input type="text" class="form-control" id="edad_exp">
              </div>
            </div>
            <br>

            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Situación Civil:</label>
                <select class="form-control"  id="situacion_civil_exp">                                
                  <?php foreach($catSituacionCivil as $situacionCivil) {?>
                    <option value="<?php echo $situacionCivil->id_situacion_civil; ?>"><?php echo $situacionCivil->valor; ?></option>                            
                  <?php } ?> 
                </select>
                <br>          
                <label class="lblBold">Embarazada:</label>&nbsp;&nbsp;
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="embarazoRadioOptions" id="radioEmbarazo1" value="1">
                    <label class="form-check-label" for="radioEmbarazo1">Sí</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="embarazoRadioOptions" id="radioEmbarazo2" value="0">
                    <label class="form-check-label" for="radioEmbarazo2">No</label>
                  </div>
              </div>
              <div class="col-md-6">
                <label class="lblBold">Expectativa:</label><br>
                <textarea class="form-control" id="expectativa_exp" rows="4" cols="40"></textarea>
              </div>
            </div>
            
            <br>
            
            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Actividad Laboral:</label><br>
                <input type="text" class="form-control" id="actividad_laboral_exp">
              </div>
              <div class="col-md-6">
                <label class="lblBold">Descripción Actividad:</label><br>
                <textarea class="form-control" id="desc_actividad_exp" rows="4" cols="40"></textarea>
              </div>
            </div>
          </form>
        </div>
        
        <br>
                               
        <!-- ANTECEDENTES  -->
        <div class="container" id="antecedentesExp">
          <form name="form" class="form-horizontal">
            <div class="row">
              <div class="col-md-4">
                <label class="lblBold">Situación Estrés:</label>                            
                <select class="form-control" id="situacion_estres_exp">
                  <?php foreach($catSituacionEstres as $situacionEstres) {?>
                    <option value="<?php echo $situacionEstres->id_estres; ?>"><?php echo $situacionEstres->valor; ?></option>                            
                  <?php } ?>                       
                </select>
              </div>
              <div class="col-md-4">
                <label class="lblBold">Situación Intestinal:</label>                            
                <select class="form-control"  id="situacion_intestinal_exp">
                  <?php foreach($catSituacionIntestinal as $situacionIntestinal) {?>
                    <option value="<?php echo $situacionIntestinal->id_intestinal; ?>"><?php echo $situacionIntestinal->valor; ?></option>                            
                  <?php } ?>                        
                </select>
              </div>
              <div class="col-md-4">
                <label class="lblBold">Situación Sueño:</label>                            
                <select class="form-control"  id="situacion_sueno_exp">
                  <?php foreach($catSituacionSueno as $situacionSueno) {?>
                    <option value="<?php echo $situacionSueno->id_sueno; ?>"><?php echo $situacionSueno->valor; ?></option>                            
                  <?php } ?>                      
                </select>
              </div>
            </div>
                        
            <br>

            <div class="row">
              <div class="col-md-4">
                <label class="lblBold">Complexión:</label><br>
                <input type="text" class="form-control" id="complexion_exp">
              </div>
              <div class="col-md-4">
                <label class="lblBold">Actividad Física:</label><br>                            
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="actFisicaRadioOptions" id="radioActFisica1" value="1">
                  <label class="form-check-label" for="radioActFisica1">Sí</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="actFisicaRadioOptions" id="radioActFisica2" value="0">
                  <label class="form-check-label" for="radioActFisica2">No</label>
                </div>
              </div>
              <div class="col-md-4">
                <label class="lblBold">Ingesta Agua:</label><br>                            
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ingestaAguaRadioOptions" id="radioIngestaAgua1" value="1">
                  <label class="form-check-label" for="radioIngestaAgua1">Sí</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="ingestaAguaRadioOptions" id="radioIngestaAgua2" value="0">
                  <label class="form-check-label" for="radioIngestaAgua2">No</label>
                </div>
              </div>
            </div>

            <br>

            <div class="row">
              <div class="col-md-4">
                <label class="lblBold">Consumo Tabaco:</label><br>                            
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="tabacoRadioOptions" id="radioTabaco1" value="1">
                  <label class="form-check-label" for="radioTabaco1">Sí</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="tabacoRadioOptions" id="radioTabaco2" value="0">
                  <label class="form-check-label" for="radioTabaco2">No</label>
                </div>
              </div>
              <div class="col-md-4">
                <label class="lblBold">Consumo Drogas:</label><br>                            
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="drogasRadioOptions" id="radioDrogas1" value="1">
                  <label class="form-check-label" for="radioDrogas1">Sí</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="drogasRadioOptions" id="radioDrogas2" value="0">
                  <label class="form-check-label" for="radioDrogas2">No</label>
                </div>
              </div>
              <div class="col-md-4">
                <label class="lblBold">Consumo Alcohol:</label><br>                            
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="alcoholRadioOptions" id="radioAlcohol1" value="1">
                  <label class="form-check-label" for="radioAlcohol1">Sí</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="alcoholRadioOptions" id="radioAlcohol2" value="0">
                  <label class="form-check-label" for="radioAlcohol2">No</label>
                </div>
              </div>
            </div>

            <br>

            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Alimentos Favoritos:</label><br>
                <textarea class="form-control" id="alimentos_fav_exp" rows="4" cols="40"></textarea>
              </div>
              <div class="col-md-6">
                <label class="lblBold">Alimentos Rechazados:</label><br>
                <textarea class="form-control" id="alimentos_rech_exp" rows="4" cols="40"></textarea>
              </div>
            </div>

            <br>

            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Control Fisiológico:</label><br>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">Dolor de Pecho</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                  <label class="form-check-label" for="defaultCheck2">Dificultad para respirar</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">Palpitaciones</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                  <label class="form-check-label" for="defaultCheck2">Aumento de Frecuencia Cardiaca</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">Aumento de Presión Arterial</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                  <label class="form-check-label" for="defaultCheck2">Dolor de cabeza y cuello</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">Fatiga</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                  <label class="form-check-label" for="defaultCheck2">Ansioso e Irritable</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">Ansiedad</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                  <label class="form-check-label" for="defaultCheck2">Sudoración</label>
                </div>
              </div>                            
            </div>

            <br>

            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Antecedentes Familiares:</label><br>
                <textarea class="form-control" id="antecedentes_fam_exp"  rows="4" cols="40"></textarea>
              </div>
              <div class="col-md-6">
                <label class="lblBold">Antecedentes Personales:</label><br>
                <textarea class="form-control" id="antecedentes_pers_exp" rows="4" cols="40"></textarea>
              </div>
            </div>
            
          </form>
        </div>

        <br>

        <!-- SOMATOMETRIA -->
        <div class="container" id="somatometriaExp">
          <form name="form" class="form-horizontal">
            <div class="row">
              <div class="col-md-3">
                <label class="lblBold">Peso</label><br>
                <input type="text" class="form-control solo-letras" id="peso_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">Altura:</label>
                <input type="text" class="form-control" id="altura_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">Talla:</label>
                <input type="text" class="form-control" id="talla_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">Cadera:</label>
                <input type="text" class="form-control"  id="cadera_exp">
              </div>
            </div>
    
            <br>

            <div class="row">
              <div class="col-md-3">
                <label class="lblBold">Muñeca</label><br>
                <input type="text" class="form-control solo-letras"  id="muneca_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">Cintura:</label>
                <input type="text" class="form-control" id="cintura_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">Pantorrila:</label>
                <input type="text" class="form-control"  id="pantorrilla_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">IMC:</label>
                <input type="text" class="form-control"  id="imc_exp">
              </div>
            </div>
            
            <br>

            <div class="row">
              <div class="col-md-3">
                <label class="lblBold">P. Abdominal</label><br>
                <input type="text" class="form-control solo-letras" id="p_abdominal_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">P. Auxiliar Medio:</label>
                <input type="text" class="form-control" id="p_aux_medio_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">P. Pectoral:</label>
                <input type="text" class="form-control" id="p_pectoral_exp">
              </div>
              <div class="col-md-3">
                <label class="lblBold">P. Subescapular:</label>
                <input type="text" class="form-control" id="p_subescapular_exp">
              </div>
            </div>
          </form>          
        </div>

        <!-- OTROS -->
        <div class="container" id="otrosExp">
          <form name="form" class="form-horizontal">
            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Etiquetas:</label><br>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <?php foreach($catEtiquetas as $etiquetas) {?>
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="<?php echo $etiquetas->id_cat_etiqueta; ?>" id="defaultCheck<?php echo $etiquetas->id_cat_etiqueta; ?>">
                    <label class="form-check-label" for="defaultCheck<?php echo $etiquetas->id_cat_etiqueta; ?>">
                      <?php echo $etiquetas->nombre; ?>
                    </label>
                  </div>
                <?php } ?> 
              </div>                          
            </div>

            <br>

            <div class="row">
              <div class="col-md-6">
                <label class="lblBold">Notas:</label><br>
                <textarea class="form-control" id="notas_exp"  rows="4" cols="40"></textarea>
              </div>
              <div class="col-md-6">
                <label class="lblBold">Observaciones:</label><br>
                <textarea class="form-control" id="observaciones_exp" rows="4" cols="40"></textarea>
              </div>
            </div>

          </form>
        </div>
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-modal" id="btnGuardarExpediente">Guardar</button>
      </div>

    </div>
  </div>
</div>