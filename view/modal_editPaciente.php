<div class="modal fade" id="modalEditarPaciente" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                    <input type="hidden" id="id_paciente_edit"> 
                      <h5 class="modal-title" id="exampleModalLongTitle">Editar Paciente</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <form name="form" class="form-horizontal">
                        <div class="row">
                          <div class="col-md-12">
                            <label class="lblBold">Nombre del paciente:</label>
                          </div><br>
                          <div class="col-md-3">
                            <input type="text" class="form-control solo-letras" name="nombre1_paciente"
                              id="nombre1_paciente_edit" placeholder="Primer Nombre">
                          </div>
                          <div class="col-md-3">
                            <input type="text" class="form-control solo-letras" name="nombre2_paciente"
                              id="nombre2_paciente_edit" placeholder="Segundo Nombre">
                          </div>
                          <div class="col-md-3">
                            <input type="text" class="form-control solo-letras" name="apellido_paterno_paciente"
                              id="apellido_paterno_paciente_edit" placeholder="Apellido Paterno">
                          </div>
                          <div class="col-md-3">
                            <input type="text" class="form-control solo-letras" name="apellido_materno_paciente"
                              id="apellido_materno_paciente_edit" placeholder="Apellido Paterno">
                          </div>
                        </div>
                        <br>
                        <div class="row">                  
                          <div class="col-md-3">
                            <label class="lblBold">Nombre común:</label><br>
                            <input type="text" class="form-control solo-letras" name="nombrecomun_paciente"
                              id="nombrecomun_paciente_edit">
                          </div>
                          <div class="col-md-3">
                            <label class="lblBold">Fecha de Nacimiento:</label>
                            <input type="date" class="form-control" name="fechaNac_paciente" id="fechaNac_paciente_edit">
                          </div>
                          <div class="col-md-3">
                            <label class="lblBold">Sexo:</label>
                            <select class="form-control" name="sexo_paciente" id="sexo_paciente_edit">
                              <option value="masculino">Masculino</option>
                              <option value="femenino">Femenino</option>
                            </select>
                          </div>
                        </div><br>
                        <div class="row">
                          <div class="col-md-12">
                            <label class="lblBold">Dirección:</label>
                          </div><br>
                          <div class="col-md-3">
                            <input type="text" class="form-control solo-letras" name="calle_paciente"
                              id="calle_paciente_edit" placeholder="Calle">
                          </div>
                          <div class="col-md-3">
                            <input type="text" class="form-control solo-letras" name="numero_paciente"
                              id="numero_paciente_edit" placeholder="Número">
                          </div>
                          <div class="col-md-3">
                            <input type="text" class="form-control solo-letras" name=colonia_paciente"
                              id="colonia_paciente_edit" placeholder="Colonia">
                          </div>
                           <div class="col-md-3">
                            <input type="text" class="form-control solo-letras" name="codigopostal_paciente"
                              id="codigopostal_paciente_edit" placeholder="Código Postal">
                          </div>                    
                        </div><br>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="lblBold">Estado:</label>
                            
                            <select class="form-control" name="estado_paciente" id="estado_paciente_edit">
                          <?php foreach($catEstados as $estado) {?>
                              <option value="<?php echo $estado->id; ?>"><?php echo $estado->estado; ?></option>                            
                            <?php } ?>                       
                            </select>
                          </div>
                          <div class="col-md-3">
                            <label class="lblBold">Ciudad:</label>
                            <select class="form-control" name="ciudad_paciente" id="ciudad_paciente_edit" > 
                            <?php foreach($catMunicipios as $municipio) {?>
                              <option value="<?php echo $municipio->id; ?>"><?php echo $municipio->municipio; ?></option>                            
                            <?php } ?>                            
                            </select>
                          </div>
                          <div class="col-md-3">
                            <label class="lblBold">Teléfono Casa:</label><br>
                            <input type="text" class="form-control solo-letras" name="telcasa_paciente"
                              id="telcasa_paciente_edit">
                          </div>
                          <div class="col-md-3">
                            <label class="lblBold">Teléfono Trabajo:</label>
                            <input type="text" class="form-control" name="teltrabajo_paciente" id="teltrabajo_paciente_edit">
                          </div>
                        </div><br>
                        <div class="row">                                            
                          <div class="col-md-3">
                            <label class="lblBold">Celular:</label>
                            <input type="text" class="form-control" name="celular_paciente" id="celular_paciente_edit">
                          </div>
                          <div class="col-md-3">
                            <label class="lblBold">Email 1:</label><br>
                            <input type="text" class="form-control solo-letras" name="email1_paciente"
                              id="email1_paciente_edit">
                          </div>
                          <div class="col-md-3">
                            <label class="lblBold">Email 2:</label>
                            <input type="text" class="form-control" name="email2_paciente" id="email2_paciente_edit">
                          </div>
                          <div class="col-md-3">
                            <label class="lblBold">Red social:</label>
                            <input type="text" class="form-control" name="redsocial_paciente" id="redsocial_paciente_edit">
                          </div>
                        </div><br>
                        <div class="row">                  
                          <div class="col-md-6">
                            <label class="lblBold">Observaciones:</label><br>
                            <textarea class="form-control" id="observaciones_paciente_edit" name="observaciones_paciente" rows="4" cols="40"></textarea>
                          </div>
                          <div class="col-md-6">
                            <label class="lblBold">Subir foto:</label><br>
                            <input type="file" name="fotopaciente" id="fotopaciente_edit">
                          </div>                      
                        </div><br>
                      </form>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <button type="button" class="btn btn-modal" id="btnGuardarCambiosPaciente">Guardar</button>
                    </div>
                  </div>
                </div>
              </div>