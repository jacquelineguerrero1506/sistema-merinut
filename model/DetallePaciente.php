<?php
class DetallePaciente extends EntidadBase{

    private $id_expediente;
    private $id_paciente;
    private $id_situacion_civil;
    private $embarazada;
    private $expectativa_motivo;
    private $actividad_laboral;
    private $descripcion_actividad;
    private $id_estres;
    private $complexion;
    private $id_intestinal;
    private $id_sueno;
    private $actividad_fisica;
    private $consumo_tabaco;
    private $consumo_alcohol;
    private $consumo_drogas;
    private $alimentos_favoritos;
    private $alimentos_rechazados;
    private $ingesta_agua;
    private $id_ctr_fisiologico;
    private $antecedentes_familiares;
    private $antecedentes_personales;
    private $medicamentos;
    private $peso;
    private $altura;
    private $talla;
    private $cadera;
    private $muneca;
    private $cintura;
    private $pantorrilla;
    private $imc;
    private $p_abdominal;
    private $p_auxiliar_medio;
    private $p_pectoral;
    private $p_subescapular;
    private $observaciones_generales;
    // DIETA
    private $id_dieta;
    private $desayuno;
    private $colacion1;
    private $comida;
    private $colacion2;
    private $cena;
    //CONSULTA
    private $id_consulta;
    //SEGUIMIENTO
    private $id_seguimiento;



    public function __construct($adapter) {
        $table = "tbl_expedientes";
        parent::__construct($table, $adapter);
    }

    /**
     * Get the value of id_expediente
     */ 
    public function getId_expediente()
    {
        return $this->id_expediente;
    }

    /**
     * Set the value of id_expediente
     *
     * @return  self
     */ 
    public function setId_expediente($id_expediente)
    {
        $this->id_expediente = $id_expediente;

        return $this;
    }

    /**
     * Get the value of id_paciente
     */ 
    public function getId_paciente()
    {
        return $this->id_paciente;
    }

    /**
     * Set the value of id_paciente
     *
     * @return  self
     */ 
    public function setId_paciente($id_paciente)
    {
        $this->id_paciente = $id_paciente;

        return $this;
    }

    /**
     * Get the value of id_situacion_civil
     */ 
    public function getId_situacion_civil()
    {
        return $this->id_situacion_civil;
    }

    /**
     * Set the value of id_situacion_civil
     *
     * @return  self
     */ 
    public function setId_situacion_civil($id_situacion_civil)
    {
        $this->id_situacion_civil = $id_situacion_civil;

        return $this;
    }

    /**
     * Get the value of embarazada
     */ 
    public function getEmbarazada()
    {
        return $this->embarazada;
    }

    /**
     * Set the value of embarazada
     *
     * @return  self
     */ 
    public function setEmbarazada($embarazada)
    {
        $this->embarazada = $embarazada;

        return $this;
    }

    /**
     * Get the value of expectativa_motivo
     */ 
    public function getExpectativa_motivo()
    {
        return $this->expectativa_motivo;
    }

    /**
     * Set the value of expectativa_motivo
     *
     * @return  self
     */ 
    public function setExpectativa_motivo($expectativa_motivo)
    {
        $this->expectativa_motivo = $expectativa_motivo;

        return $this;
    }

    /**
     * Get the value of actividad_laboral
     */ 
    public function getActividad_laboral()
    {
        return $this->actividad_laboral;
    }

    /**
     * Set the value of actividad_laboral
     *
     * @return  self
     */ 
    public function setActividad_laboral($actividad_laboral)
    {
        $this->actividad_laboral = $actividad_laboral;

        return $this;
    }

    /**
     * Get the value of descripcion_actividad
     */ 
    public function getDescripcion_actividad()
    {
        return $this->descripcion_actividad;
    }

    /**
     * Set the value of descripcion_actividad
     *
     * @return  self
     */ 
    public function setDescripcion_actividad($descripcion_actividad)
    {
        $this->descripcion_actividad = $descripcion_actividad;

        return $this;
    }

    /**
     * Get the value of id_estres
     */ 
    public function getId_estres()
    {
        return $this->id_estres;
    }

    /**
     * Set the value of id_estres
     *
     * @return  self
     */ 
    public function setId_estres($id_estres)
    {
        $this->id_estres = $id_estres;

        return $this;
    }

    /**
     * Get the value of complexion
     */ 
    public function getComplexion()
    {
        return $this->complexion;
    }

    /**
     * Set the value of complexion
     *
     * @return  self
     */ 
    public function setComplexion($complexion)
    {
        $this->complexion = $complexion;

        return $this;
    }

    /**
     * Get the value of id_intestinal
     */ 
    public function getId_intestinal()
    {
        return $this->id_intestinal;
    }

    /**
     * Set the value of id_intestinal
     *
     * @return  self
     */ 
    public function setId_intestinal($id_intestinal)
    {
        $this->id_intestinal = $id_intestinal;

        return $this;
    }

    /**
     * Get the value of id_sueno
     */ 
    public function getId_sueno()
    {
        return $this->id_sueno;
    }

    /**
     * Set the value of id_sueno
     *
     * @return  self
     */ 
    public function setId_sueno($id_sueno)
    {
        $this->id_sueno = $id_sueno;

        return $this;
    }

    /**
     * Get the value of actividad_fisica
     */ 
    public function getActividad_fisica()
    {
        return $this->actividad_fisica;
    }

    /**
     * Set the value of actividad_fisica
     *
     * @return  self
     */ 
    public function setActividad_fisica($actividad_fisica)
    {
        $this->actividad_fisica = $actividad_fisica;

        return $this;
    }

    /**
     * Get the value of consumo_tabaco
     */ 
    public function getConsumo_tabaco()
    {
        return $this->consumo_tabaco;
    }

    /**
     * Set the value of consumo_tabaco
     *
     * @return  self
     */ 
    public function setConsumo_tabaco($consumo_tabaco)
    {
        $this->consumo_tabaco = $consumo_tabaco;

        return $this;
    }

    /**
     * Get the value of consumo_alcohol
     */ 
    public function getConsumo_alcohol()
    {
        return $this->consumo_alcohol;
    }

    /**
     * Set the value of consumo_alcohol
     *
     * @return  self
     */ 
    public function setConsumo_alcohol($consumo_alcohol)
    {
        $this->consumo_alcohol = $consumo_alcohol;

        return $this;
    }

    /**
     * Get the value of consumo_drogas
     */ 
    public function getConsumo_drogas()
    {
        return $this->consumo_drogas;
    }

    /**
     * Set the value of consumo_drogas
     *
     * @return  self
     */ 
    public function setConsumo_drogas($consumo_drogas)
    {
        $this->consumo_drogas = $consumo_drogas;

        return $this;
    }

    /**
     * Get the value of alimentos_favoritos
     */ 
    public function getAlimentos_favoritos()
    {
        return $this->alimentos_favoritos;
    }

    /**
     * Set the value of alimentos_favoritos
     *
     * @return  self
     */ 
    public function setAlimentos_favoritos($alimentos_favoritos)
    {
        $this->alimentos_favoritos = $alimentos_favoritos;

        return $this;
    }

    /**
     * Get the value of alimentos_rechazados
     */ 
    public function getAlimentos_rechazados()
    {
        return $this->alimentos_rechazados;
    }

    /**
     * Set the value of alimentos_rechazados
     *
     * @return  self
     */ 
    public function setAlimentos_rechazados($alimentos_rechazados)
    {
        $this->alimentos_rechazados = $alimentos_rechazados;

        return $this;
    }

    /**
     * Get the value of ingesta_agua
     */ 
    public function getIngesta_agua()
    {
        return $this->ingesta_agua;
    }

    /**
     * Set the value of ingesta_agua
     *
     * @return  self
     */ 
    public function setIngesta_agua($ingesta_agua)
    {
        $this->ingesta_agua = $ingesta_agua;

        return $this;
    }

    /**
     * Get the value of id_ctr_fisiologico
     */ 
    public function getId_ctr_fisiologico()
    {
        return $this->id_ctr_fisiologico;
    }

    /**
     * Set the value of id_ctr_fisiologico
     *
     * @return  self
     */ 
    public function setId_ctr_fisiologico($id_ctr_fisiologico)
    {
        $this->id_ctr_fisiologico = $id_ctr_fisiologico;

        return $this;
    }

    /**
     * Get the value of antecedentes_familiares
     */ 
    public function getAntecedentes_familiares()
    {
        return $this->antecedentes_familiares;
    }

    /**
     * Set the value of antecedentes_familiares
     *
     * @return  self
     */ 
    public function setAntecedentes_familiares($antecedentes_familiares)
    {
        $this->antecedentes_familiares = $antecedentes_familiares;

        return $this;
    }

    /**
     * Get the value of antecedentes_personales
     */ 
    public function getAntecedentes_personales()
    {
        return $this->antecedentes_personales;
    }

    /**
     * Set the value of antecedentes_personales
     *
     * @return  self
     */ 
    public function setAntecedentes_personales($antecedentes_personales)
    {
        $this->antecedentes_personales = $antecedentes_personales;

        return $this;
    }

    /**
     * Get the value of medicamentos
     */ 
    public function getMedicamentos()
    {
        return $this->medicamentos;
    }

    /**
     * Set the value of medicamentos
     *
     * @return  self
     */ 
    public function setMedicamentos($medicamentos)
    {
        $this->medicamentos = $medicamentos;

        return $this;
    }

    /**
     * Get the value of peso
     */ 
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set the value of peso
     *
     * @return  self
     */ 
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get the value of altura
     */ 
    public function getAltura()
    {
        return $this->altura;
    }

    /**
     * Set the value of altura
     *
     * @return  self
     */ 
    public function setAltura($altura)
    {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get the value of talla
     */ 
    public function getTalla()
    {
        return $this->talla;
    }

    /**
     * Set the value of talla
     *
     * @return  self
     */ 
    public function setTalla($talla)
    {
        $this->talla = $talla;

        return $this;
    }

    /**
     * Get the value of cadera
     */ 
    public function getCadera()
    {
        return $this->cadera;
    }

    /**
     * Set the value of cadera
     *
     * @return  self
     */ 
    public function setCadera($cadera)
    {
        $this->cadera = $cadera;

        return $this;
    }

    /**
     * Get the value of muneca
     */ 
    public function getMuneca()
    {
        return $this->muneca;
    }

    /**
     * Set the value of muneca
     *
     * @return  self
     */ 
    public function setMuneca($muneca)
    {
        $this->muneca = $muneca;

        return $this;
    }

    /**
     * Get the value of cintura
     */ 
    public function getCintura()
    {
        return $this->cintura;
    }

    /**
     * Set the value of cintura
     *
     * @return  self
     */ 
    public function setCintura($cintura)
    {
        $this->cintura = $cintura;

        return $this;
    }

    /**
     * Get the value of pantorrilla
     */ 
    public function getPantorrilla()
    {
        return $this->pantorrilla;
    }

    /**
     * Set the value of pantorrilla
     *
     * @return  self
     */ 
    public function setPantorrilla($pantorrilla)
    {
        $this->pantorrilla = $pantorrilla;

        return $this;
    }

    /**
     * Get the value of imc
     */ 
    public function getImc()
    {
        return $this->imc;
    }

    /**
     * Set the value of imc
     *
     * @return  self
     */ 
    public function setImc($imc)
    {
        $this->imc = $imc;

        return $this;
    }

    /**
     * Get the value of p_abdominal
     */ 
    public function getP_abdominal()
    {
        return $this->p_abdominal;
    }

    /**
     * Set the value of p_abdominal
     *
     * @return  self
     */ 
    public function setP_abdominal($p_abdominal)
    {
        $this->p_abdominal = $p_abdominal;

        return $this;
    }

    /**
     * Get the value of p_auxiliar_medio
     */ 
    public function getP_auxiliar_medio()
    {
        return $this->p_auxiliar_medio;
    }

    /**
     * Set the value of p_auxiliar_medio
     *
     * @return  self
     */ 
    public function setP_auxiliar_medio($p_auxiliar_medio)
    {
        $this->p_auxiliar_medio = $p_auxiliar_medio;

        return $this;
    }

    /**
     * Get the value of p_pectoral
     */ 
    public function getP_pectoral()
    {
        return $this->p_pectoral;
    }

    /**
     * Set the value of p_pectoral
     *
     * @return  self
     */ 
    public function setP_pectoral($p_pectoral)
    {
        $this->p_pectoral = $p_pectoral;

        return $this;
    }

    /**
     * Get the value of p_subescapular
     */ 
    public function getP_subescapular()
    {
        return $this->p_subescapular;
    }

    /**
     * Set the value of p_subescapular
     *
     * @return  self
     */ 
    public function setP_subescapular($p_subescapular)
    {
        $this->p_subescapular = $p_subescapular;

        return $this;
    }

    /**
     * Get the value of observaciones_generales
     */ 
    public function getObservaciones_generales()
    {
        return $this->observaciones_generales;
    }

    /**
     * Set the value of observaciones_generales
     *
     * @return  self
     */ 
    public function setObservaciones_generales($observaciones_generales)
    {
        $this->observaciones_generales = $observaciones_generales;

        return $this;
    }


    // DIETA
    public function getId_dieta()
    {
        return $this->id_dieta;
    }

    public function setId_dieta($id_dieta)
    {
        $this->id_dieta = $id_dieta;

        return $this;
    }

    public function getDesayuno()
    {
        return $this->desayuno;
    }

    public function setDesayuno($desayuno)
    {
        $this->desayuno = $desayuno;

        return $this;
    }

    public function getColacion1()
    {
        return $this->colacion1;
    }

    public function setColacion1($colacion1)
    {
        $this->colacion1 = $colacion1;

        return $this;
    }

    public function getComida()
    {
        return $this->comida;
    }

    public function setComida($comida)
    {
        $this->comida = $comida;

        return $this;
    }

    public function getColacion2()
    {
        return $this->colacion2;
    }

    public function setColacion2($colacion2)
    {
        $this->colacion2 = $colacion2;

        return $this;
    }

    public function getCena()
    {
        return $this->cena;
    }

    public function setCena($cena)
    {
        $this->cena = $cena;

        return $this;
    }

    // CONSULTA
    public function getId_consulta()
    {
        return $this->id_consulta;
    }

    public function setId_consulta($id_consulta)
    {
        $this->id_consulta = $id_consulta;

        return $this;
    }

    // SEGUIMIENTO
    public function getId_seguimiento()
    {
        return $this->getId_seguimiento;
    }

    public function setId_seguimiento($id_seguimiento)
    {
        $this->getId_seguimiento = $id_seguimiento;

        return $this;
    }




    public function save(){
        $query="INSERT INTO `tbl_expedientes`(`id_paciente`, `id_situacion_civil`, `embarazada`, `expectativa_motivo`, 
        `actividad_laboral`, `descripcion_actividad`, `id_estres`, `complexion`, `id_intestinal`, `id_sueno`, `actividad_fisica`, `consumo_tabaco`, 
        `consumo_alcohol`, `consumo_drogas`, `alimentos_favoritos`, `alimentos_rechazados`, `ingesta_agua`, `id_ctr_fisiologico`, 
        `antecedentes_familiares`, `antecedentes_personales`, `medicamentos`, `peso`, `altura`, `talla`, `cadera`, `muneca`, `cintura`, `pantorrilla`,
        `imc`, `p_abdominal`, `p_auxiliar_medio`, `p_pectoral`, `p_subescapular`, `observaciones_generales`) VALUES (
                '".$this->id_paciente."',
                '".$this->id_situacion_civil."',
                '".$this->embarazada."',
                '".$this->expectativa_motivo."',
                '".$this->actividad_laboral."',
                '".$this->descripcion_actividad."',
                '".$this->id_estres."',
                '".$this->complexion."',
                '".$this->id_intestinal."',
                '".$this->id_sueno."',
                '".$this->actividad_fisica."',
                '".$this->consumo_tabaco."',
                '".$this->consumo_alcohol."',
                '".$this->consumo_drogas."',
                '".$this->alimentos_favoritos."',
                '".$this->alimentos_rechazados."',
                '".$this->ingesta_agua."',
                '".$this->id_ctr_fisiologico."',
                '".$this->antecedentes_familiares."',
                '".$this->antecedentes_personales."',
                '".$this->medicamentos."',
                '".$this->peso."',
                '".$this->altura."',
                '".$this->talla."',
                '".$this->cadera."',
                '".$this->muneca."',
                '".$this->cintura."',
                '".$this->pantorrilla."',
                '".$this->imc."',
                '".$this->p_abdominal."',
                '".$this->p_auxiliar_medio."',
                '".$this->p_pectoral."',
                '".$this->p_subescapular."',
                '".$this->observaciones_generales."');";

        $save=$this->db()->query($query);
        $this->db()->error;
        return $save;
    }



    public function guardarDieta(){
        $query="INSERT INTO `tbl_dietas`(`id_paciente`, `id_consulta`, `desayuno`, `colacion1`, `comida`, `colacion2`, `cena`) 
                VALUES (
                '".$this->id_paciente."',
                '".$this->id_consulta."',
                '".$this->desayuno."',
                '".$this->colacion1."',
                '".$this->comida."',
                '".$this->colacion2."',                
                '".$this->cena."');";

        $saveDieta=$this->db()->query($query);
        $this->db()->error;
        return $saveDieta;
    }


    public function guardarSeguimiento(){
        $query="INSERT INTO `tbl_seguimientos`(`id_paciente`, `id_consulta`, `peso`, `altura`, `talla`, `cadera`, `muneca`, `cintura`, `pantorrilla`,
                 `imc`, `p_abdominal`, `p_auxiliar_medio`, `p_pectoral`, `p_subescapular`) VALUES (
                '".$this->id_paciente."',
                '".$this->id_consulta."',
                '".$this->peso."',
                '".$this->altura."',
                '".$this->talla."',
                '".$this->cadera."',   
                '".$this->muneca."',
                '".$this->cintura."',
                '".$this->pantorrilla."',
                '".$this->imc."',
                '".$this->p_abdominal."',
                '".$this->p_auxiliar_medio."',
                '".$this->p_pectoral."',         
                '".$this->p_subescapular."');";

        $saveSeguimiento=$this->db()->query($query);
        $this->db()->error;
        return $saveSeguimiento;
    }
}


?>
