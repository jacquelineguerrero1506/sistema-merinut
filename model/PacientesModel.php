<?php
class PacientesModel extends EntidadBase{
    private $table;
    
    public function __construct($adapter){
        $this->table = "tbl_pacientes";
        parent::__construct($this->table, $adapter);
    }
    
    //Metodos de consulta
    public function getPacienteById($id){
        $query = "SELECT * FROM tbl_pacientes WHERE id_paciente=$id";
        error_log($query);
        $paciente = $this->ejecutarSql($query);
        return $paciente;
    }


    public function getCatSituacionCivil(){
        $query = "SELECT * FROM cat_situacion";
        error_log($query);
        $situacion = $this->ejecutarSql($query);
        return $situacion;
    }

}
?>
