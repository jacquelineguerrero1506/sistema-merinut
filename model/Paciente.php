<?php
class Paciente extends EntidadBase{
    private $id_paciente;
    private $nombre1_paciente;
    private $nombre2_paciente;
    private $apellido_paterno;
    private $apellido_materno;
    private $nombre_comun;
    private $sexo;
    private $fecha_nacimiento;
    private $calle;
    private $numero;
    private $colonia;
    private $codigo_postal;
    private $id_estado;
    private $id_municipio;
    private $telefono_casa;
    private $telefono_trabajo;
    private $celular;
    private $email1;
    private $email2;
    private $red_social;
    private $id_usuario;
    private $observaciones;
    private $estatus;
    private $usuario_registra;
    private $fecha_registro;
    private $hora_registro;

    
    public function __construct($adapter) {
        $table = "tbl_pacientes";
        parent::__construct($table, $adapter);
    }
    
    public function getId_paciente(){
        return $this->id_paciente;
    }
    
    public function setId_paciente($id_paciente){
        $this->id_paciente = $id_paciente;
    }


    public function getNombre1_paciente(){
        return $this->nombre1_paciente;
    }
    
    public function setNombre1_paciente($nombre1_paciente){
        $this->nombre1_paciente = $nombre1_paciente;
    }


    public function getNombre2_paciente(){
        return $this->nombre2_paciente;
    }

    public function setNombre2_paciente($nombre2_paciente){
        $this->nombre2_paciente = $nombre2_paciente;
    }

    public function getApellido_paterno(){
            return $this->apellido_paterno;
        }

    public function setApellido_paterno($apellido_paterno){
        $this->apellido_paterno = $apellido_paterno;
    }

    public function getApellido_materno(){
            return $this->apellido_materno;
        }

    public function setApellido_materno($apellido_materno){
        $this->apellido_materno = $apellido_materno;
    }

    public function getNombre_comun(){
            return $this->nombre_comun;
        }

    public function setNombre_comun($nombre_comun){
        $this->nombre_comun = $nombre_comun;
    }

    public function getSexo(){
            return $this->sexo;
        }

    public function setSexo($sexo){
        $this->sexo = $sexo;
    }

    public function getFecha_nacimiento(){
            return $this->fecha_nacimiento;
        }

    public function setFecha_nacimiento($fecha_nacimiento){
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    public function getCalle(){
            return $this->calle;
        }

    public function setCalle($calle){
        $this->calle = $calle;
    }

    public function getNumero(){
            return $this->numero;
        }

    public function setNumero($numero){
        $this->numero = $numero;
    }

    public function getColonia(){
            return $this->colonia;
        }

    public function setColonia($colonia){
        $this->colonia = $colonia;
    }

    public function getCodigo_postal(){
            return $this->codigo_postal;
        }

    public function setCodigo_postal($codigo_postal){
        $this->codigo_postal = $codigo_postal;
    }

    public function getId_estado(){
            return $this->id_estado;
        }

    public function setId_estado($id_estado){
        $this->id_estado = $id_estado;
    }

    public function getId_municipio(){
            return $this->id_municipio;
        }

    public function setId_municipio($id_municipio){
        $this->id_municipio = $id_municipio;
    }

    public function getTelefono_casa(){
            return $this->telefono_casa;
        }

    public function setTelefono_casa($telefono_casa){
        $this->telefono_casa = $telefono_casa;
    }

    public function getTelefono_trabajo(){
            return $this->telefono_trabajo;
        }

    public function setTelefono_trabajo($telefono_trabajo){
        $this->telefono_trabajo = $telefono_trabajo;
    }

    public function getCelular(){
            return $this->celular;
        }

    public function setCelular($celular){
        $this->celular = $celular;
    }

    public function getEmail1(){
            return $this->email1;
        }

    public function setEmail1($email1){
        $this->email1 = $email1;
    }

    public function getEmail2(){
            return $this->email2;
        }

    public function setEmail2($email2){
        $this->email2 = $email2;
    }

    public function getRed_social(){
            return $this->red_social;
        }

    public function setRed_social($red_social){
        $this->red_social = $red_social;
    }

    public function getId_usuario(){
            return $this->id_usuario;
        }

    public function setId_usuario($id_usuario){
        $this->id_usuario = $id_usuario;
    }

    public function getObservaciones(){
            return $this->observaciones;
        }

    public function setObservaciones($observaciones){
        $this->observaciones = $observaciones;
    }

    public function getEstatus(){
        return $this->estatus;
    }

    public function setEstatus($estatus){
        $this->estatus = $estatus;
    }

    public function getUsuario_registra(){
            return $this->usuario_registra;
        }

    public function setUsuario_registra($usuario_registra){
        $this->usuario_registra = $usuario_registra;
    }

    public function getFecha_registro(){
            return $this->fecha_registro;
        }

    public function setFecha_registro($fecha_registro){
        $this->fecha_registro = $fecha_registro;
    }

    public function getHora_registro(){
            return $this->hora_registro;
        }

    public function setHora_registro($hora_registro){
        $this->hora_registro = $hora_registro;
    }


    public function save(){
        $query="INSERT INTO tbl_pacientes (`nombre1_paciente`, `nombre2_paciente`, `apellido_paterno`, `apellido_materno`, 
                `nombre_comun`, `sexo`, `fecha_nacimiento`, `calle`, `numero`, `colonia`, `codigo_postal`, `id_estado`, `id_municipio`, 
                `telefono_casa`, `telefono_trabajo`, `celular`, `email1`, `email2`, `red_social`, `id_usuario`, `observaciones`, `estatus`, 
                `usuario_registra`, `fecha_registro`, `hora_registro`) VALUES (
                '".$this->nombre1_paciente."',
                '".$this->nombre2_paciente."',
                '".$this->apellido_paterno."',
                '".$this->apellido_materno."',
                '".$this->nombre_comun."',
                '".$this->sexo."',
                '".$this->fecha_nacimiento."',
                '".$this->calle."',
                '".$this->numero."',
                '".$this->colonia."',
                '".$this->codigo_postal."',
                '".$this->id_estado."',
                '".$this->id_municipio."',
                '".$this->telefono_casa."',
                '".$this->telefono_trabajo."',
                '".$this->celular."',
                '".$this->email1."',
                '".$this->email2."',
                '".$this->red_social."',
                NULL,
                '".$this->observaciones."',
                '".$this->estatus."',
                '".$this->usuario_registra."',
                '".$this->fecha_registro."',
                '".$this->hora_registro."');";

                error_log($query);
        $save=$this->db()->query($query);
        $this->db()->error;
        return $save;
    }


    public function update(){
        $query="UPDATE tbl_pacientes SET 
                nombre1_paciente='".$this->nombre1_paciente."',
                nombre2_paciente = '".$this->nombre2_paciente."',
                apellido_paterno = '".$this->apellido_paterno."',
                apellido_materno = '".$this->apellido_materno."',
                nombre_comun = '".$this->nombre_comun."',
                sexo = '".$this->sexo."',
                fecha_nacimiento = '".$this->fecha_nacimiento."',
                calle = '".$this->calle."',
                numero = '".$this->numero."',
                colonia = '".$this->colonia."',
                codigo_postal = '".$this->codigo_postal."',
                id_estado = '".$this->id_estado."',
                id_municipio = '".$this->id_municipio."',
                telefono_casa = '".$this->telefono_casa."',
                telefono_trabajo = '".$this->telefono_trabajo."',
                celular = '".$this->celular."',
                email1 = '".$this->email1."',
                email2 = '".$this->email2."',
                red_social = '".$this->red_social."',
                id_usuario = NULL,
                observaciones = '".$this->observaciones."'
                WHERE id_paciente = '".$this->id_paciente."'";

                error_log($query);
        $save=$this->db()->query($query);
        $this->db()->error;
        return $save; 
    }

}
?>