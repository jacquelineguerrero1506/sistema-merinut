<?php
class DetallePacienteController extends ControladorBase{

    public $conectar;
	public $adapter;
	
    public function __construct() {
        parent::__construct();
		 
        $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
    }
    
    public function index(){
       
        
        //Creamos el objeto paciente
        $detPaciente = new DetallePaciente($this->adapter);
        
        //Conseguimos todos los pacientes
     //   $allpacientes = $paciente->getAll('paciente');
       
        //CATALOGO DE ESTADOS
        $catSituacionCivil = $detPaciente->getCatalogo('cat_situacion');
        $catSituacionEstres = $detPaciente->getCatalogo('cat_estres');
        $catSituacionIntestinal = $detPaciente->getCatalogo('cat_intestinal');
        $catSituacionSueno = $detPaciente->getCatalogo('cat_sueno');
        $catEtiquetas = $detPaciente->getCatalogo('cat_etiquetas');


         //CATALOGO DE ESTADOS
      //   $catMunicipios = $paciente->getCatalogo('cat_municipios');

        //Cargamos la vista index y le pasamos valores
        $this->view("detallePaciente",array(            
            "catSituacionCivil" => $catSituacionCivil,
            "catSituacionEstres" => $catSituacionEstres,
            "catSituacionIntestinal" => $catSituacionIntestinal,
            "catSituacionSueno" => $catSituacionSueno,
            "catEtiquetas" => $catEtiquetas
        ));
        
    }
   
    public function guardarExpediente(){

        if(isset($_POST["idPaciente"])){
            
            //Creamos un paciente
            $paciente = new DetallePaciente($this->adapter);
            $paciente -> setId_paciente($_POST["idPaciente"]);
            $paciente -> setId_situacion_civil($_POST["situacionCivil"]);
            $paciente -> setEmbarazada($_POST["embarazo"]);
            $paciente -> setExpectativa_motivo($_POST["expectativa"]);
            $paciente -> setActividad_laboral($_POST["actividadLaboral"]);
            $paciente -> setDescripcion_actividad($_POST["descripcionActividad"]);
            $paciente -> setId_estres($_POST["estres"]);
            $paciente -> setId_intestinal($_POST["intestinal"]);
            $paciente -> setId_sueno($_POST["sueno"]);
            $paciente -> setComplexion($_POST["complexion"]);
            $paciente -> setActividad_fisica($_POST["actividadFisica"]);
            $paciente -> setIngesta_agua($_POST["ingestaAgua"]);
            $paciente -> setConsumo_tabaco($_POST["tabaco"]);
            $paciente -> setConsumo_drogas($_POST["drogas"]);
            $paciente -> setConsumo_alcohol($_POST["alcohol"]);
            $paciente -> setAlimentos_favoritos($_POST["alimentosFavoritos"]);
            $paciente -> setAlimentos_rechazados($_POST["alimentosRechazados"]);
            $paciente -> setAntecedentes_familiares($_POST["antecedentesFamiliares"]);
            $paciente -> setAntecedentes_personales($_POST["antecedentesPersonales"]);
            $paciente -> setPeso($_POST["peso"]);
            $paciente -> setAltura($_POST["altura"]);
            $paciente -> setTalla($_POST["talla"]);
            $paciente -> setCadera($_POST["cadera"]);
            $paciente -> setMuneca($_POST["muneca"]);
            $paciente -> setCintura($_POST["cintura"]);
            $paciente -> setPantorrilla($_POST["pantorrilla"]);
            $paciente -> setImc($_POST["imc"]);
            $paciente -> setP_abdominal($_POST["p_abdominal"]);
            $paciente -> setP_auxiliar_medio($_POST["p_auxMedio"]);
            $paciente -> setP_pectoral($_POST["p_pectoral"]);
            $paciente -> setP_subescapular($_POST["p_subescapular"]);
            $paciente -> setObservaciones_generales($_POST["observaciones"]);

            
             $save = $paciente -> save();
        }

        echo $this->returnJson(200, $_POST);
        exit;
    }


    public function guardarDieta(){

        if(isset($_POST["idPaciente"])){
            
            $paciente = new DetallePaciente($this->adapter);
            $paciente -> setId_paciente($_POST["idPaciente"]);
            $paciente -> setId_consulta($_POST["idConsulta"]);
            $paciente -> setDesayuno($_POST["desayuno"]);
            $paciente -> setColacion1($_POST["colacion1"]);
            $paciente -> setComida($_POST["comida"]);
            $paciente -> setColacion2($_POST["colacion2"]);
            $paciente -> setCena($_POST["cena"]);   
            
             $save = $paciente -> guardarDieta();
        }

        echo $this->returnJson(200, $_POST);
        exit;
    }

/*
    public function editarPaciente(){

        if(isset($_POST["nombre1"])){
            
            //Creamos un paciente
            $paciente = new Paciente($this->adapter);
            $paciente -> setId_paciente($_POST["idPaciente"]);
            $paciente -> setNombre1_paciente($_POST["nombre1"]);
            $paciente -> setNombre2_paciente($_POST["nombre2"]);
            $paciente -> setApellido_paterno($_POST["apePaterno"]);
            $paciente -> setApellido_materno($_POST["apeMaterno"]);
            $paciente -> setNombre_comun($_POST["nombreComun"]);
            $paciente -> setSexo($_POST["sexo"]);
            $paciente -> setFecha_nacimiento($_POST["fechaNacimiento"]);
            $paciente -> setCalle($_POST["calle"]);
            $paciente -> setNumero($_POST["numero"]);
            $paciente -> setColonia($_POST["colonia"]);
            $paciente -> setCodigo_postal($_POST["codigoPostal"]);
            $paciente -> setId_estado($_POST["idEstado"]);
            $paciente -> setId_municipio($_POST["idCiudad"]);
            $paciente -> setTelefono_casa($_POST["telCasa"]);
            $paciente -> setTelefono_trabajo($_POST["telTrabajo"]);
            $paciente -> setCelular($_POST["celular"]);
            $paciente -> setEmail1($_POST["email1"]);
            $paciente -> setEmail2($_POST["email2"]);
            $paciente -> setRed_social($_POST["redSocial"]);
            $paciente -> setId_usuario($_POST["idUsuario"]);
            $paciente -> setObservaciones($_POST["observaciones"]);
            
            $save = $paciente -> update();
        }

        echo $this->returnJson(200, $_POST);
        exit;
        //$this->redirect("Pacientes", "index");
    }
 
    public function obtenerCiudadxEstado(){

        $idEstado = $_POST["idEstado"];
       
        $estados =  new Paciente($this->adapter);
        $catCiudades = $estados->obtenerCiudadxEstado($idEstado);

        echo $this->returnJson(200, $catCiudades);
        exit;       
    }
*/
    public function obtenerExpedientePacienteById(){

        $idPaciente = $_POST["id_paciente"];

        $pacientes =  new DetallePaciente($this->adapter);
        $expedientePaciente = $pacientes->getDetallePAcienteById($idPaciente);

        echo $this->returnJson(200, $expedientePaciente);
        exit;       
    }


    public function obtenerCatSituacionCivil(){
        error_log("obtenerCatSituacionCivil");
        $pacientes =  new PacientesModel($this->adapter);
        $catSituacionCivil = $pacientes->getCatSituacionCivil();

        echo $this->returnJson(200, $catSituacionCivil);
        exit;    
    }
    
    
    public function obtenerDietaPaciente(){

        $idPaciente = $_POST["id_paciente"];

        $pacientes =  new DetallePaciente($this->adapter);
        $dietaPaciente = $pacientes->getDietaPaciente($idPaciente);

   //     error_log(print_r($dietaPaciente, true));
        echo $this->returnJson(200, $dietaPaciente);
        exit;       
    }

    public function guardarSeguimiento(){

        if(isset($_POST["idPaciente"])){
            
            $paciente = new DetallePaciente($this->adapter);
            $paciente -> setId_paciente($_POST["idPaciente"]);
            $paciente -> setId_consulta($_POST["idConsulta"]);
            $paciente -> setPeso($_POST["peso"]);
            $paciente -> setAltura($_POST["altura"]);
            $paciente -> setTalla($_POST["talla"]);
            $paciente -> setCadera($_POST["cadera"]);
            $paciente -> setMuneca($_POST["muneca"]);  
            $paciente -> setCintura($_POST["cintura"]);   
            $paciente -> setPantorrilla($_POST["pantorrilla"]);   
            $paciente -> setImc($_POST["imc"]);   
            $paciente -> setP_abdominal($_POST["p_abdominal"]);   
            $paciente -> setP_auxiliar_medio($_POST["p_aux_medio"]);   
            $paciente -> setP_pectoral($_POST["p_pectoral"]);   
            $paciente -> setP_subescapular($_POST["p_subescapular"]);   

            $save = $paciente -> guardarSeguimiento();
        }

        echo $this->returnJson(200, $_POST);
        exit;
    }


    //OBTENER INFO SEGUIMIENTO
    public function obtenerSeguimiento(){

        $idPaciente = $_POST["id_paciente"];

        $pacientes =  new DetallePaciente($this->adapter);
        $seguimiento = $pacientes->getSeguimiento($idPaciente);

        error_log(print_r($seguimiento, true));
        echo $this->returnJson(200, $seguimiento);
        exit;       
    }
/*
    public function eliminarPaciente(){
        $idPaciente = $_POST["id_paciente"];

        $pacientes =  new Paciente($this->adapter);
        $eliminarPaciente = $pacientes->deleteById($idPaciente, 'paciente');

        echo $this->returnJson(200, $eliminarPaciente);
        exit; 
    }
*/
}
?>
