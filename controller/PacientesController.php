<?php
class PacientesController extends ControladorBase{
    public $conectar;
	public $adapter;
	
    public function __construct() {
        parent::__construct();
		 
        $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
    }
    
    public function index(){
        
        //Creamos el objeto paciente
        $paciente = new Paciente($this->adapter);
        
        //Conseguimos todos los pacientes
        $allpacientes = $paciente->getAll('paciente');
       
        //CATALOGO DE ESTADOS
        $catEstados = $paciente->getCatalogo('cat_estados');

         //CATALOGO DE ESTADOS
         $catMunicipios = $paciente->getCatalogo('cat_municipios');

        //Cargamos la vista index y le pasamos valores
        $this->view("pacientes",array(
            "allpacientes" => $allpacientes,
            "catEstados" => $catEstados,
            "catMunicipios" => $catMunicipios
        ));
    }
    
    public function registrarPaciente(){

        if(isset($_POST["nombre1"])){
            
            //Creamos un paciente
            $paciente = new Paciente($this->adapter);
            $paciente -> setNombre1_paciente($_POST["nombre1"]);
            $paciente -> setNombre2_paciente($_POST["nombre2"]);
            $paciente -> setApellido_paterno($_POST["apePaterno"]);
            $paciente -> setApellido_materno($_POST["apeMaterno"]);
            $paciente -> setNombre_comun($_POST["nombreComun"]);
            $paciente -> setSexo($_POST["sexo"]);
            $paciente -> setFecha_nacimiento($_POST["fechaNacimiento"]);
            $paciente -> setCalle($_POST["calle"]);
            $paciente -> setNumero($_POST["numero"]);
            $paciente -> setColonia($_POST["colonia"]);
            $paciente -> setCodigo_postal($_POST["codigoPostal"]);
            $paciente -> setId_estado($_POST["idEstado"]);
            $paciente -> setId_municipio($_POST["idCiudad"]);
            $paciente -> setTelefono_casa($_POST["telCasa"]);
            $paciente -> setTelefono_trabajo($_POST["telTrabajo"]);
            $paciente -> setCelular($_POST["celular"]);
            $paciente -> setEmail1($_POST["email1"]);
            $paciente -> setEmail2($_POST["email2"]);
            $paciente -> setRed_social($_POST["redSocial"]);
            $paciente -> setId_usuario($_POST["idUsuario"]);
            $paciente -> setObservaciones($_POST["observaciones"]);
            $paciente -> setEstatus($_POST["estatus"]);
            $paciente -> setUsuario_registra($_POST["idUsuarioRegistra"]);
            $paciente -> setFecha_registro($_POST["fechaRegistro"]);
            $paciente -> setHora_registro($_POST["horaRegistro"]);
            
            $save = $paciente -> save();
        }

        echo $this->returnJson(200, $_POST);
        exit;
        //$this->redirect("Pacientes", "index");
    }

    public function editarPaciente(){

        if(isset($_POST["nombre1"])){
            
            //Creamos un paciente
            $paciente = new Paciente($this->adapter);
            $paciente -> setId_paciente($_POST["idPaciente"]);
            $paciente -> setNombre1_paciente($_POST["nombre1"]);
            $paciente -> setNombre2_paciente($_POST["nombre2"]);
            $paciente -> setApellido_paterno($_POST["apePaterno"]);
            $paciente -> setApellido_materno($_POST["apeMaterno"]);
            $paciente -> setNombre_comun($_POST["nombreComun"]);
            $paciente -> setSexo($_POST["sexo"]);
            $paciente -> setFecha_nacimiento($_POST["fechaNacimiento"]);
            $paciente -> setCalle($_POST["calle"]);
            $paciente -> setNumero($_POST["numero"]);
            $paciente -> setColonia($_POST["colonia"]);
            $paciente -> setCodigo_postal($_POST["codigoPostal"]);
            $paciente -> setId_estado($_POST["idEstado"]);
            $paciente -> setId_municipio($_POST["idCiudad"]);
            $paciente -> setTelefono_casa($_POST["telCasa"]);
            $paciente -> setTelefono_trabajo($_POST["telTrabajo"]);
            $paciente -> setCelular($_POST["celular"]);
            $paciente -> setEmail1($_POST["email1"]);
            $paciente -> setEmail2($_POST["email2"]);
            $paciente -> setRed_social($_POST["redSocial"]);
            $paciente -> setId_usuario($_POST["idUsuario"]);
            $paciente -> setObservaciones($_POST["observaciones"]);
            
            $save = $paciente -> update();
        }

        echo $this->returnJson(200, $_POST);
        exit;
        //$this->redirect("Pacientes", "index");
    }
 
    public function obtenerCiudadxEstado(){

        $idEstado = $_POST["idEstado"];
       
        $estados =  new Paciente($this->adapter);
        $catCiudades = $estados->obtenerCiudadxEstado($idEstado);

        echo $this->returnJson(200, $catCiudades);
        exit;       
    }

    public function obtenerPacienteById(){

        $idPaciente = $_POST["id_paciente"];

        $pacientes =  new Paciente($this->adapter);
        $datosPaciente = $pacientes->getById($idPaciente, 'paciente');

        echo $this->returnJson(200, $datosPaciente);
        exit;       
    }

    public function eliminarPaciente(){
        $idPaciente = $_POST["id_paciente"];

        $pacientes =  new Paciente($this->adapter);
        $eliminarPaciente = $pacientes->deleteById($idPaciente, 'paciente');

        echo $this->returnJson(200, $eliminarPaciente);
        exit; 
    }



    public function subirImagen(){
        if (($_FILES["file"]["type"] == "image/pjpeg")
            || ($_FILES["file"]["type"] == "image/jpeg")
            || ($_FILES["file"]["type"] == "image/png")) {
            if (move_uploaded_file($_FILES["file"]["tmp_name"], "./img/".$_FILES['file']['name'])) {
                //more code here...
                echo "./img/".$_FILES['file']['name'];
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

}
?>
