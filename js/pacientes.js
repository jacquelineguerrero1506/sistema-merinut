(function(){

    $(document).ready(function(){

        // OBTENER CIUDADES DE ACUERDO AL ESTADO SELECCIONADO
        $(document).on("change", "#estado_paciente , #estado_paciente_edit", function(){
           
            var selectActual = ($(this).attr("id"));  
            var id = '"#'+selectActual+'"';
            var idEstado = $(id).val();    
           
            

            console.log("Estado " + idEstado + "   select " + selectActual);
            $.ajax({
                url: './?controller=Pacientes&action=obtenerCiudadxEstado',
                type: 'POST',
                cache: false,
                data: {
                    idEstado: idEstado
                },
                timeout: 10000,
                success: function (res){


                    var selectCiudades = selectActual = "estado_paciente" ? $("#ciudad_paciente") : $("#ciudad_paciente_edit");  
                    console.log(selectCiudades);              
                    var ciudad = res.data;

                    if (selectActual = "estado_paciente"){
                        $("#ciudad_paciente").empty();
                    } else {
                        $("#ciudad_paciente_edit").empty();
                    }
                    

                    $.each(ciudad, function(i, item) {                        
                        selectCiudades.append('<option value=' + ciudad[i].municipios_id + '>' + ciudad[i].municipio + '</option>');
                    });

                },
                error: function (e){
                    console.log('error');
                    console.log(e);
                },
                complete: function (){
                    console.log('complete');
                }
            });
        });

        
        //REALIZAR ACCIONES 
        $(document).on("change", "#acciones_paciente", function(){

            var opcionSelec = $("#acciones_paciente option:selected").attr('data-id');
            var name = $("#acciones_paciente option:selected").attr('data-name'); 
            console.log(name);
            switch($("#acciones_paciente option:selected").val()){                
               

                case "edit":
                    obtenerPacienteById(opcionSelec);
                    $("#modalEditarPaciente").modal();
                    $("#estado_paciente_edit").change();
                    break;
                
                case "det":
                    console.log("detalle");
                    break;
                
                case "exp":
                    var a = document.createElement('a');   
                    a.setAttribute("href", "./?controller=DetallePaciente&accion=index");
                    a.click();  
                    break;

                case "del":
                    eliminarPaciente(opcionSelec);
                    break;                
            }             
        });

        
        // GUARDAR REGISTRO PACIENTE
        $(document).on("click", "#btnGuardarPaciente", function(){

            var hoy = new Date();
            var fecha = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + (hoy.getDate());
            var hora = hoy.getHours()+":"+hoy.getMinutes()+":"+hoy.getSeconds()+":"+hoy.getMilliseconds();

            $.ajax({
                url: './?controller=Pacientes&action=registrarPaciente',
                type: 'POST',
                cache: false,
                data: {
                    nombre1: $('#nombre1_paciente').val(),
                    nombre2: $('#nombre2_paciente').val(),
                    apePaterno: $('#apellido_paterno_paciente').val(),
                    apeMaterno: $('#apellido_materno_paciente').val(),
                    nombreComun: $('#nombrecomun_paciente').val(),
                    fechaNacimiento: $('#fechaNac_paciente').val(),
                    sexo: $('#sexo_paciente').val(),
                    calle: $('#calle_paciente').val(),
                    numero: $('#numero_paciente').val(),
                    colonia: $('#colonia_paciente').val(),
                    codigoPostal: $('#codigopostal_paciente').val(),
                    idEstado: $('#estado_paciente').val(),
                    idCiudad: $('#ciudad_paciente').val(),
                    telCasa: $('#telcasa_paciente').val(),
                    telTrabajo: $('#teltrabajo_paciente').val(),
                    celular: $('#celular_paciente').val(),
                    email1: $('#email1_paciente').val(),
                    email2: $('#email2_paciente').val(),
                    redSocial: $('#redsocial_paciente').val(),
                    idUsuario: 'NULL',
                    observaciones: $('#observaciones_paciente').val(),
                    estatus: 1,
                    foto: $('#fotopaciente').val(),
                    idUsuarioRegistra: NULL,
                    fechaRegistro: fecha,
                    horaRegistro: hora
                },
                timeout: 10000,
                success: function (data){
                    console.log(data);
                    Swal.fire(
                        'Bien hecho!',
                        '¡Paciente insertado correctamente!',
                        'success'
                      )                    
                },
                error: function (e){
                    console.log('error');
                    console.log(e);
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Algo salió mal al intentar guardar'
                      })
                },
                complete: function (){
                    console.log('complete');
                }
            })
        });


        // EDITAR REGISTRO PACIENTE
        $(document).on("click", "#btnGuardarCambiosPaciente", function(){

           /* var hoy = new Date();
            var fecha = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + (hoy.getDate());
            var hora = hoy.getHours()+":"+hoy.getMinutes()+":"+hoy.getSeconds()+":"+hoy.getMilliseconds();
            */
            $.ajax({
                url: './?controller=Pacientes&action=editarPaciente',
                type: 'POST',
                cache: false,
                data: {
                    idPaciente: $('#id_paciente_edit').val(),
                    nombre1: $('#nombre1_paciente_edit').val(),
                    nombre2: $('#nombre2_paciente_edit').val(),
                    apePaterno: $('#apellido_paterno_paciente_edit').val(),
                    apeMaterno: $('#apellido_materno_paciente_edit').val(),
                    nombreComun: $('#nombrecomun_paciente_edit').val(),
                    fechaNacimiento: $('#fechaNac_paciente_edit').val(),
                    sexo: $('#sexo_paciente_edit').val(),
                    calle: $('#calle_paciente_edit').val(),
                    numero: $('#numero_paciente_edit').val(),
                    colonia: $('#colonia_paciente_edit').val(),
                    codigoPostal: $('#codigopostal_paciente_edit').val(),
                    idEstado: $('#estado_paciente_edit').val(),
                    idCiudad: $('#ciudad_paciente_edit').val(),
                    telCasa: $('#telcasa_paciente_edit').val(),
                    telTrabajo: $('#teltrabajo_paciente_edit').val(),
                    celular: $('#celular_paciente_edit').val(),
                    email1: $('#email1_paciente_edit').val(),
                    email2: $('#email2_paciente_edit').val(),
                    redSocial: $('#redsocial_paciente_edit').val(),
                    idUsuario: 'NULL',
                    observaciones: $('#observaciones_paciente_edit').val(),
                    foto: $('#fotopaciente_edit').val()
                },
                timeout: 10000,
                success: function (data){
                    console.log(data);
                    Swal.fire(
                        'Bien hecho!',
                        '¡Paciente modificado correctamente!',
                        'success'
                      )                    
                },
                error: function (e){
                    console.log('error');
                    console.log(e);
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Algo salió mal al intentar guardar'
                      })
                },
                complete: function (){
                    console.log('complete');
                }
            })
        });


    // ELIMINAR REGISTRO PACIENTE
    function eliminarPaciente(id){
        Swal.fire({
            title: '¿Estás seguro de que deseas eliminar el registro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, Eliminar',
            cancelButtonText: "Cancelar"
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: './?controller=Pacientes&action=eliminarPaciente',
                    type: 'POST',
                    cache: false,
                    data: {
                        id_paciente: id
                    },
                    timeout: 10000,
                    success: function (data){
                        console.log('success');
                        console.log(data);
                        Swal.fire(
                            'Registro Eliminado',
                            'El registro se eliminó con éxito.',
                            'success'
                          );
                    },
                    error: function (e){
                        console.log('error');
                        console.log(e);
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Algo salió mal al intentar eliminar'
                          })
                    },
                    complete: function (){
                        console.log('complete');
                        // window.location.reload();
                        
                    }
                })            
            }
           
          });
         
    }

    //OBTENER INFORMACIÓN DE UN PACIENTE
    function obtenerPacienteById(id){
        console.log(id);
        
        $.ajax({
            url: './?controller=Pacientes&action=obtenerPacienteById',
            type: 'POST',
            cache: false,
            data: {
                id_paciente: id
            },
            timeout: 10000,
            success: function (resp){
                console.log('success');
                console.log(resp);

                $('#id_paciente_edit').val(resp.data['id_paciente']),
                $('#nombre1_paciente_edit').val(resp.data['nombre1_paciente']),
                $('#nombre2_paciente_edit').val(resp.data['nombre2_paciente']),
                $('#apellido_paterno_paciente_edit').val(resp.data['apellido_paterno']),
                $('#apellido_materno_paciente_edit').val(resp.data['apellido_materno']),
                $('#nombrecomun_paciente_edit').val(resp.data['nombre_comun']),
                $('#fechaNac_paciente_edit').val(resp.data['fecha_nacimiento']),
                $('#sexo_paciente_edit').val(resp.data['sexo']),
                $('#calle_paciente_edit').val(resp.data['calle']),
                $('#numero_paciente_edit').val(resp.data['numero']),
                $('#colonia_paciente_edit').val(resp.data['colonia']),
                $('#codigopostal_paciente_edit').val(resp.data['codigo_postal']),
                $('#estado_paciente_edit').val(resp.data['id_estado']),
                $('#ciudad_paciente_edit').val(resp.data['id_municipio']),
                $('#telcasa_paciente_edit').val(resp.data['telefono_casa']),
                $('#teltrabajo_paciente_edit').val(resp.data['telefono_trabajo']),
                $('#celular_paciente_edit').val(resp.data['celular']),
                $('#email1_paciente_edit').val(resp.data['email1']),
                $('#email2_paciente_edit').val(resp.data['email2']),
                $('#redsocial_paciente_edit').val(resp.data['red_social']),
                $('#observaciones_paciente_edit').val(resp.data['observaciones']),
                $('#fotopaciente_edit').val()
            },
            error: function (e){
                console.log('error');
                console.log(e);
            },
            complete: function (){
                console.log('complete');
            }
        }); 
    }





    $("#upload").on('click', function() {
        var formData = new FormData();
        var files = $('#fotopaciente')[0].files[0];


        formData.append('file',files);

        
        var codificado = btoa(formData);
        console.log(codificado);

        $.ajax({
            url: './?controller=Pacientes&action=subirImagen',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    $(".card-img").attr("src", response);
                } else {
                    alert('Formato de imagen incorrecto.');
                }
            }
        });
        return false;
    });



    
});


})(jQuery);