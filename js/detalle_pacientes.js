(function(){

    $(document).ready(function(){

        obtenerExpedientePaciente();
        obtenerDietaPaciente();
        obtenerSeguimiento();

        $('#tabInfoGeneral').addClass("active");

        $('#antecedentesExp').hide();
        $('#somatometriaExp').hide();
        $('#otrosExp').hide();

        $(document).on("click", "#tabAntecedentes", function(){
            $(this).addClass("active");
            $('#tabInfoGeneral').removeClass("active");
            $('#tabSomatometria').removeClass("active");
            $('#tabOtros').removeClass("active");

            $('#antecedentesExp').show();
            $('#infoGeneralExp').hide();
            $('#somatometriaExp').hide();
            $('#otrosExp').hide();
                   
        });

        $(document).on("click", "#tabInfoGeneral", function(){
            $(this).addClass("active");
            $('#tabAntecedentes').removeClass("active");
            $('#tabSomatometria').removeClass("active");
            $('#tabOtros').removeClass("active");

            $('#antecedentesExp').hide();
            $('#infoGeneralExp').show();
            $('#somatometriaExp').hide();
            $('#otrosExp').hide();
        });

        $(document).on("click", "#tabSomatometria", function(){
            $(this).addClass("active");
            $('#tabAntecedentes').removeClass("active");
            $('#tabInfoGeneral').removeClass("active");
            $('#tabOtros').removeClass("active");

            $('#antecedentesExp').hide();
            $('#infoGeneralExp').hide();
            $('#somatometriaExp').show();
            $('#otrosExp').hide();
                   
        });

        $(document).on("click", "#tabOtros", function(){
            $(this).addClass("active");
            $('#tabAntecedentes').removeClass("active");
            $('#tabSomatometria').removeClass("active");
            $('#tabInfoGeneral').removeClass("active");

            $('#antecedentesExp').hide();
            $('#infoGeneralExp').hide();
            $('#somatometriaExp').hide();
            $('#otrosExp').show();
        });

                //REALIZAR ACCIONES 
                $(document).on("click", "#btnRegistrarExpediente", function(){
                    console.log('click');
                      $("#modalExpediente").modal();
                           
                });
        

                //GUARDAR DETALLE PACIENTE
                $(document).on("click", "#btnGuardarExpediente", function(){
        
                    $.ajax({
                        url: './?controller=DetallePaciente&action=guardarExpediente',
                        type: 'POST',
                        cache: false,
                        data: {
                            idPaciente: 1,
                            situacionCivil: $('#situacion_civil_exp').val(),
                            embarazo: $('input:radio[name=embarazoRadioOptions]:checked').val(),
                            expectativa: $('#expectativa_exp').val(),
                            actividadLaboral: $('#actividad_laboral_exp').val(),
                            descripcionActividad: $('#desc_actividad_exp').val(),
                            estres: $('#situacion_estres_exp').val(),
                            intestinal: $('#situacion_intestinal_exp').val(),
                            sueno: $('#situacion_sueno_exp').val(),
                            complexion: $('#complexion_exp').val(),
                            actividadFisica: $('input:radio[name=actFisicaRadioOptions]:checked').val(),
                            ingestaAgua: $('input:radio[name=ingestaAguaRadioOptions]:checked').val(),
                            tabaco: $('input:radio[name=tabacoRadioOptions]:checked').val(),
                            drogas: $('input:radio[name=drogasRadioOptions]:checked').val(),
                            alcohol: $('input:radio[name=alcoholRadioOptions]:checked').val(),
                            alimentosFavoritos: $('#alimentos_fav_exp').val(),
                            alimentosRechazados: $('#alimentos_rech_exp').val(),
                            antecedentesFamiliares: $('#antecedentes_fam_exp').val(),
                            antecedentesPersonales: $('#antecedentes_pers_exp').val(),
                            peso: $('#peso_exp').val(),
                            altura: $('#altura_exp').val(),
                            talla: $('#talla_exp').val(),
                            cadera: $('#cadera_exp').val(),
                            muneca: $('#muneca_exp').val(),
                            cintura: $('#cintura_exp').val(),
                            pantorrilla: $('#pantorrilla_exp').val(),
                            imc: $('#imc_exp').val(),
                            p_abdominal: $('#p_abdominal_exp').val(),
                            p_auxMedio: $('#p_aux_medio_exp').val(),
                            p_pectoral: $('#p_pectoral_exp').val(),
                            p_subescapular: $('#p_subescapular_exp').val(),
                            notas: $('#notas_exp').val(),
                            observaciones: $('#observaciones_exp').val()
                        },
                        timeout: 10000,
                        success: function (data){
                            console.log(data);
                            Swal.fire(
                                'Bien hecho!',
                                'Expediente registrado correctamente!',
                                'success'
                              )                    
                        },
                        error: function (e){
                            console.log('error');
                            console.log(e);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Algo salió mal al intentar guardar'
                              })
                        },
                        complete: function (){
                            console.log('complete');
                        }
                    })
                });
      

        
                //GUARDAR DIETA
                $(document).on("click", "#btnGuardarDieta", function(){
        
                    $.ajax({
                        url: './?controller=DetallePaciente&action=guardarDieta',
                        type: 'POST',
                        cache: false,
                        data: {
                            idPaciente: 1,
                            idConsulta:1,
                            desayuno: $('#desayuno').val(),
                            colacion1: $('#colacion1').val(),
                            comida: $('#comida').val(),
                            colacion2: $('#colacion1').val(),
                            cena: $('#cena').val()
                        },
                        timeout: 10000,
                        success: function (data){
                            console.log(data);
                            Swal.fire(
                                'Bien hecho!',
                                'Dieta guardada correctamente!',
                                'success'
                              )                    
                        },
                        error: function (e){
                            console.log('error');
                            console.log(e);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Algo salió mal al intentar guardar'
                              })
                        },
                        complete: function (){
                            console.log('complete');
                        }
                    })
                });

        
        function obtenerExpedientePaciente(){

            console.log($('#id_paciente_exp').val());
            
            
            $.ajax({
                url: './?controller=DetallePaciente&action=obtenerExpedientePacienteById',
                type: 'POST',
                cache: false,
                data: {
                    id_paciente: 1//$('#id_paciente_exp').val()
                },
                timeout: 10000,
                success: function (resp){
                    console.log('success');
                    console.log(resp);

                    if (resp.data[0].embarazada == 1){
                        $("input[type='radio'][name='ver_embarazoRadioOptions'][value='1']").prop('checked',true);
                    }else{
                        $("input[type='radio'][name='ver_embarazoRadioOptions'][value='0']").prop('checked',true);
                    }

                    if (resp.data[0].actividad_fisica == 1){
                        $("input[type='radio'][name='ver_actFisicaRadioOptions'][value='1']").prop('checked',true);
                    }else{
                        $("input[type='radio'][name='ver_actFisicaRadioOptions'][value='0']").prop('checked',true);
                    }

                    if (resp.data[0].ingesta_agua == 1){
                        $("input[type='radio'][name='ver_ingestaAguaRadioOptions'][value='1']").prop('checked',true);
                    }else{
                        $("input[type='radio'][name='ver_ingestaAguaRadioOptions'][value='0']").prop('checked',true);
                    }

                    if (resp.data[0].consumo_tabaco == 1){
                        $("input[type='radio'][name='ver_tabacoRadioOptions'][value='1']").prop('checked',true);
                    }else{
                        $("input[type='radio'][name='ver_tabacoRadioOptions'][value='0']").prop('checked',true);
                    }

                    if (resp.data[0].consumo_drogas == 1){
                        $("input[type='radio'][name='ver_drogasRadioOptions'][value='1']").prop('checked',true);
                    }else{
                        $("input[type='radio'][name='ver_drogasRadioOptions'][value='0']").prop('checked',true);
                    }

                    if (resp.data[0].consumo_alcohol == 1){
                        $("input[type='radio'][name='ver_alcoholRadioOptions'][value='1']").prop('checked',true);
                    }else{
                        $("input[type='radio'][name='ver_alcoholRadioOptions'][value='0']").prop('checked',true);
                    }
                    
                    $('#ver_nombre_paciente_exp').val(resp.data[0].nombre),
                    $('#ver_situacion_civil_exp').val(resp.data[0].id_situacion_civil),
                    $('#ver_expectativa_exp').val(resp.data[0].expectativa_motivo),
                    $('#ver_actividad_laboral_exp').val(resp.data[0].actividad_laboral),
                    $('#ver_desc_actividad_exp').val(resp.data[0].descripcion_actividad),
                    $('#ver_situacion_estres_exp').val(resp.data[0].id_estres),
                    $('#ver_situacion_intestinal_exp').val(resp.data[0].id_intestinal),
                    $('#ver_situacion_sueno_exp').val(resp.data[0].id_sueno),
                    $('#ver_complexion_exp').val(resp.data[0].complexion),
                    $('#ver_alimentos_fav_exp').val(resp.data[0].alimentos_favoritos),
                    $('#ver_alimentos_rech_exp').val(resp.data[0].alimentos_rechazados),
                    $('#ver_antecedentes_fam_exp').val(resp.data[0].antecedentes_familiares),
                    $('#ver_antecedentes_pers_exp').val(resp.data[0].antecedentes_personales),
                    $('#ver_peso_exp').val(resp.data[0].peso),
                    $('#ver_altura_exp').val(resp.data[0].altura),
                    $('#ver_talla_exp').val(resp.data[0].talla),
                    $('#ver_cadera_exp').val(resp.data[0].cadera),
                    $('#ver_muneca_exp').val(resp.data[0].muneca),
                    $('#ver_cintura_exp').val(resp.data[0].cintura),
                    $('#ver_pantorrilla_exp').val(resp.data[0].pantorrilla),
                    $('#ver_imc_exp').val(resp.data[0].imc),
                    $('#ver_p_abdominal_exp').val(resp.data[0].p_abdominal),
                    $('#ver_p_aux_medio_exp').val(resp.data[0].p_auxiliar_medio),
                    $('#ver_p_pectoral_exp').val(resp.data[0].p_pectoral),
                    $('#ver_p_subescapular_exp').val(resp.data[0].p_subescapular),                    
                    $('#ver_observaciones_exp').val(resp.data[0].observaciones_generales)
                    
                },
                error: function (e){
                    console.log('error');
                    console.log(e);
                },
                complete: function (){
                    console.log('complete');
                }
            }); 
        

        }


        function obtenerDietaPaciente(){

            console.log($('#id_paciente_exp').val());
            
            
            $.ajax({
                url: './?controller=DetallePaciente&action=obtenerDietaPaciente',
                type: 'POST',
                cache: false,
                data: {
                    id_paciente: 1//$('#id_paciente_exp').val()
                },
                timeout: 10000,
                success: function (resp){
                    console.log('obtenerDietaPaciente');
                    console.log(resp);
                  
                    $('#verDesayuno').val(resp.data[0].desayuno),
                    $('#verColacion1').val(resp.data[0].colacion1),
                    $('#verComida').val(resp.data[0].comida),
                    $('#verColacion2').val(resp.data[0].colacion2),
                    $('#verCena').val(resp.data[0].cena)    
                    
                },
                error: function (e){
                    console.log('error');
                    console.log(e);
                },
                complete: function (){
                    console.log('complete');
                }
            }); 
        

        }



         //GUARDAR SEGUIMIENTO
         $(document).on("click", "#btnGuardarSeguimiento", function(){
        
            $.ajax({
                url: './?controller=DetallePaciente&action=guardarSeguimiento',
                type: 'POST',
                cache: false,
                data: {
                    idPaciente: 1,
                    idConsulta:1,
                    peso: $('#seg_peso').val(),
                    altura: $('#seg_altura').val(),
                    talla: $('#seg_talla').val(),
                    cadera: $('#seg_cadera').val(),
                    muneca: $('#seg_muneca').val(),
                    cintura: $('#seg_cintura').val(),
                    pantorrilla: $('#seg_pantorrilla').val(),
                    imc: $('#seg_imc').val(),
                    p_abdominal: $('#seg_pabdominal').val(),
                    p_aux_medio: $('#seg_pauxmedio').val(),
                    p_pectoral: $('#seg_ppectoral').val(),
                    p_subescapular: $('#seg_psubescapular').val()
                },
                timeout: 10000,
                success: function (data){
                    console.log(data);
                    Swal.fire(
                        'Bien hecho!',
                        'Información de seguimiento guardada correctamente!',
                        'success'
                      )                    
                },
                error: function (e){
                    console.log('error');
                    console.log(e);
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Algo salió mal al intentar guardar'
                      })
                },
                complete: function (){
                    console.log('complete');
                }
            })
        });


        function obtenerSeguimiento(){

            console.log($('#id_paciente_exp').val());
            
            
            $.ajax({
                url: './?controller=DetallePaciente&action=obtenerSeguimiento',
                type: 'POST',
                cache: false,
                data: {
                    id_paciente: 1//$('#id_paciente_exp').val()
                },
                timeout: 10000,
                success: function (resp){
                    console.log('obtenerSeguimiento');
                    console.log(resp);

                    var valoresTabla = '<tr><td>16/Julio/2020</td><td>'+ resp.data[0].peso +'</td><td>'+ resp.data[0].altura +'</td>'+
                                       '<td>'+ resp.data[0].talla +'</td><td>'+ resp.data[0].cadera +'</td><td>'+ resp.data[0].muneca +'</td>'+
                                       '<td>'+ resp.data[0].cintura +'</td><td>'+ resp.data[0].pantorrilla +'</td><td>'+ resp.data[0].imc +'</td>'+
                                       '<td>'+ resp.data[0].p_abdominal +'</td><td>'+ resp.data[0].p_auxiliar_medio +'</td>'+
                                       '<td>'+ resp.data[0].p_pectoral +'</td><td>'+ resp.data[0].p_subescapular +'</td></tr>';

                  
                    $('#tblSeguimiento tbody').append(valoresTabla);
                    
                },
                error: function (e){
                    console.log('error');
                    console.log(e);
                },
                complete: function (){
                    console.log('complete');
                }
            }); 
        

        }


    
});


})(jQuery);