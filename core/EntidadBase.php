<?php
class EntidadBase{
    private $table;
    private $db;
    private $conectar;

    public function __construct($table, $adapter) {
        $this->table=(string) $table;
        
		/*
        require_once 'Conectar.php';
        $this->conectar=new Conectar();
        $this->db=$this->conectar->conexion();
		 */
		$this->conectar = null;
		$this->db = $adapter;
    }
    
    public function getConectar(){
        return $this->conectar;
    }
    
    public function db(){
        return $this->db;
    }
    
    public function getAll($desc_id){
        $query=$this->db->query("SELECT * FROM $this->table where estatus = 1 ORDER BY id_$desc_id DESC");

        while ($row = $query->fetch_object()) {
           $resultSet[]=$row;
        }
        
        return $resultSet;
    }
    
    public function getById($id, $desc_id){
        $query=$this->db->query("SELECT * FROM $this->table WHERE id_$desc_id=$id");

        if($row = $query->fetch_object()) {
           $resultSet=$row;
        }
        
        return $resultSet;
    }
    
    public function getBy($column,$value){
        $query=$this->db->query("SELECT * FROM $this->table WHERE $column='$value'");

        while($row = $query->fetch_object()) {
           $resultSet[]=$row;
        }
        
        return $resultSet;
    }
    
    public function deleteById($id, $desc_id){
        $query=$this->db->query("UPDATE $this->table SET estatus=0 WHERE id_$desc_id=$id"); 
        return $query;
    }
    
    public function deleteBy($column,$value){
        $query=$this->db->query("DELETE FROM $this->table WHERE $column='$value'"); 
        return $query;
    }
    

    public function getCatalogo($catalogo){
        $query=$this->db->query("SELECT * FROM $catalogo");

        while ($row = $query->fetch_object()) {
           $resultSet[]=$row;
        }
        
        return $resultSet;
    }


    public function obtenerCiudadxEstado($idEstado){

        $query = $this->db->query("SELECT exm.municipios_id, m.municipio FROM ctr_estados_municipios exm 
                                    JOIN cat_municipios m ON (m.id = exm.municipios_id) WHERE estados_id = $idEstado");

                    while ($row = $query->fetch_object()) {
                        $resultSet[]=$row;
                     }
                     
                     return $resultSet;
        
    }


    public function getDetallePAcienteById($idPaciente){

        $query = $this->db->query("SELECT e.*, CONCAT(p.nombre1_paciente, ' ', p.nombre2_paciente, ' ', p.apellido_paterno, ' ', p.apellido_materno) AS nombre 
                                FROM tbl_expedientes e JOIN tbl_pacientes p ON e.id_paciente = p.id_paciente WHERE e.id_paciente = $idPaciente");

                    while ($row = $query->fetch_object()) {
                        $resultSet[]=$row;
                     }
                     
                     return $resultSet;
    }


    
    public function getDietaPaciente($idPaciente){

        $query = $this->db->query("SELECT id_dieta, id_paciente, id_consulta, desayuno, colacion1, comida, colacion2, cena FROM tbl_dietas 
                                    WHERE id_paciente = $idPaciente");

                    while ($row = $query->fetch_object()) {
                        $resultSet[]=$row;
                     }
                     
                     return $resultSet;
    }


    public function getSeguimiento($idPaciente){

        $query = $this->db->query("SELECT id_seguimiento, id_paciente, id_consulta, peso, altura, talla, cadera, muneca, cintura, pantorrilla, imc, 
                                p_abdominal , p_auxiliar_medio, p_pectoral, p_subescapular FROM tbl_seguimientos WHERE id_paciente = $idPaciente");

                    while ($row = $query->fetch_object()) {
                        $resultSet[]=$row;
                     }
                     
                     return $resultSet;
    }

    /*
     * Aqui podemos montarnos un monton de métodos que nos ayuden
     * a hacer operaciones con la base de datos de la entidad
     */
    
}
?>
